<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tracking_no', 'eta', 'registry_no', 'carrier_name', 'vessel_name', 'warehouse', 'container_no', 'exchange_rate', 'point_of_discharge', 'remarks', 'user_id', 'manually_added_by', 'updated_by'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Get the user who imported the shipment records.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}