<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipment;

// Laravel Excel
use Excel;

use Carbon\Carbon;

class ExportShipment extends Controller
{
    /**
     * Export shipment data to file(.xlsx or .csv).
     *
     * @param $type
     * @return File file
     */
    public function downloadFile($type){
        $shipments = Shipment::all([
                                    'tracking_no AS TrackingNumber',
                                    'eta AS ETA',
                                    'registry_no AS RegistryNumber',
                                    'carrier_name AS CarrierName',
                                    'vessel_name AS VesselName',
                                    'warehouse AS Warehouse',
                                    'container_no AS ContainerNumber',
                                    'exchange_rate AS ExchangeRate',
                                    'point_of_discharge AS PointOfDischarge',
                                    'remarks AS Remarks'
                                ]);

        return Excel::create('shipment-' . Carbon::now(), function($excel) use ($shipments) {
            $excel->setCreator('Paccord Inc.');
            $excel->sheet('Shipment', function($sheet) use ($shipments)
            {
                $sheet->fromArray($shipments);
            });
        })->download($type);
    }
}
