<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserAccountRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return User::paginate(5);
        // return User::where('id', '!=', '1')->paginate(5);

        if(\Auth::id() == 1){
            return ['users' => User::paginate(5), 'auth_id' => \Auth::id()];
        }
        return ['users' => User::where('id', '!=', '1')->paginate(5), 'auth_id' => \Auth::id()];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserAccountRequest $request)
    {
        if($request->validated()){
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            if($user){
                $message = [
                    'msgTitle' => 'User Created!',
                    'msgBody' => 'User successfully created!',
                    'dialogType' => 'success'
                ];

                return response()->json($message, 200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $user = User::find(\Auth::id());

        // Check if the current password is corrent.
        if(Hash::check($request->current_password, $user->password)){
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);

            if($user->save()){
                $message = [
                    'msgTitle' => 'Profile Updated!',
                    'msgBody' => 'Your profile has been updated!',
                    'dialogType' => 'success'
                ];

                return response()->json($message, 201);
            }
        } else {
            $message = [
                'msgTitle' => 'Login Info Error!',
                'msgBody' => 'The current password you provided is incorrect.',
                'dialogType' => 'error'
            ];

            return response()->json($message, 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user = User::destroy($user->id);

        if($user){
            $message = [
                'msgTitle' => 'Deleted!',
                'msgBody' => 'The user has been deleted.',
                'dialogType' => 'success'
            ];

            return response()->json($message, 201);
        }
    }
}
