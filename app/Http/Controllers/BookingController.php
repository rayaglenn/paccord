<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\BookingMail;
use Illuminate\Validation\Rule;

class BookingController extends Controller
{
    /**
     * Send an email to La Jolla admin.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendBooking(Request $request)
    {

        $this->validate($request, [
            'type'    => 'required',
            'name'    => 'required',
            'email'   => 'required|email',
            'subject'   => 'required',
            'message' => 'required',
        ]);

        // Send email to Paccord official email address to send booking inquiries.
        \Mail::to('rayaglenn@gmail.com')->send(new BookingMail($request));

        $response = [
            'msgTitle' => 'Booking Sent',
            'msgBody' => "Your message was sent! We'll get back to you shortly.",
            'dialogType' => "success"
        ];

        return response()->json($response, 200);
        
    }
}
