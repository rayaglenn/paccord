<?php

namespace App\Http\Controllers;

use App\Shipment;
use Illuminate\Http\Request;
use App\Http\Requests\ShipmentRequest;
use Carbon\Carbon;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Shipment::where('tracking_no', 'like',  '%' . $request->search . '%')
                           ->orWhere('eta', $request->search)
                           ->orWhere('registry_no', 'like',  '%' . $request->search . '%')
                           ->orWhere('carrier_name', 'like',  '%' . $request->search . '%')
                           ->orWhere('vessel_name', 'like',  '%' . $request->search . '%')
                           ->orWhere('warehouse', 'like',  '%' . $request->search . '%')
                           ->orWhere('container_no', 'like',  '%' . $request->search . '%')
                           ->orWhere('exchange_rate', 'like',  '%' . $request->search . '%')
                           ->orWhere('point_of_discharge', 'like',  '%' . $request->search . '%')
                           ->orWhere('remarks', 'like',  '%' . $request->search . '%')
                           ->orderBy($request->sortField, $request->sortType)
                           ->paginate(10);
        // if($request->search && !isset($request->eta)){
        //     return Shipment::where('tracking_no', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('eta', $request->search)
        //                    ->orWhere('registry_no', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('carrier_name', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('vessel_name', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('warehouse', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('container_no', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('exchange_rate', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('point_of_discharge', 'like',  '%' . $request->search . '%')
        //                    ->orWhere('remarks', 'like',  '%' . $request->search . '%')
        //                    ->orderBy($request->sortField, $request->sortType)
        //                    ->paginate(10);
        // } else if ($request->search && $request->eta){
        //     return Shipment::where('eta', $request->search)
        //                    ->orderBy($request->sortField, $request->sortType)
        //                    ->paginate(10);
        // } else {
        //     return Shipment::orderBy($request->sortField, $request->sortType)
        //                    ->paginate(10);
        // }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShipmentRequest $request)
    {
        $shipment = new Shipment();
        $shipment->tracking_no = $request->trackingNumber;
        $shipment->eta = Carbon::parse($request->eta);
        $shipment->registry_no = $request->registryNumber;
        $shipment->carrier_name = $request->carrierName;
        $shipment->vessel_name = $request->vesselName;
        $shipment->warehouse = $request->warehouse;
        $shipment->container_no = $request->containerNumber;
        $shipment->exchange_rate = $request->exchangeRate;
        $shipment->point_of_discharge = $request->pointOfDischarge;
        $shipment->remarks = $request->remarks;
        $shipment->user_id = \Auth::id();
        $shipment->updated_by = 0;

        if($shipment->save()){
            $message = [
                'msgTitle' => 'Success',
                'msgBody' => 'A new shipment details has been added.',
                'dialogType' => 'success'
            ];
            return response()->json($message, 201);
        } else {
            return $request->validated();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Shipment $shipment)
    {
        $shipment = Shipment::where('tracking_no', $request->tracking_no)->first();

        if($shipment){
            return $shipment;
        } else {
            $message = [
                'msgTitle' => 'Not Found',
                'msgBody' => 'There are no shipment in our database with that tracking #, please try again later.',
                'dialogType' => 'error'
            ];

            return response()->json($message, 201);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function edit(Shipment $shipment)
    {
        return $shipment;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function update(ShipmentRequest $request, Shipment $shipment)
    {
        $shipment = Shipment::find($request->id);

        $shipment->tracking_no = $request->trackingNumber;
        $shipment->eta = Carbon::parse($request->eta);
        $shipment->registry_no = $request->registryNumber;
        $shipment->carrier_name = $request->carrierName;
        $shipment->vessel_name = $request->vesselName;
        $shipment->warehouse = $request->warehouse;
        $shipment->container_no = $request->containerNumber;
        $shipment->exchange_rate = $request->exchangeRate;
        $shipment->point_of_discharge = $request->pointOfDischarge;
        $shipment->remarks = $request->remarks;
        $shipment->user_id = $request->user_id;
        $shipment->updated_by = \Auth::id();

        if($shipment->save()){
            $message = [
                'msgTitle' => 'Success',
                'msgBody' => 'The shipment details has been updated.',
                'dialogType' => 'success'
            ];
            return response()->json($message, 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipment $shipment)
    {
        $shipment = Shipment::destroy($shipment->id);

        if($shipment){
            $message = [
                'msgTitle' => 'Deleted',
                'msgBody' => 'The shipment has been deleted.',
                'dialogType' => 'success'
            ];

            return response()->json($message, 201);
        }
    }
}
