<?php

namespace App\Http\Controllers;

use App\Shipment;
use App\Http\Requests\ImportCSVRequest;
use Illuminate\Http\Request;

use Illuminate\Http\File;

// Storage Facade
use Storage;

// DateTime Class
use Carbon\Carbon;

// Excel
use Excel;

class ImportShipmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Import .csv shipment file to the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImportCSVRequest $request)
    {
        if($request->validated()){
            $csv = Storage::putFile('public', new File($request->file('file')));

            $path = 'storage/' . basename(Storage::url($csv));
            $data = Excel::load($path)->get();

            if($data->count()){
                foreach ($data as $key => $value) {
                    try {
                        $eta = str_replace("/", "-", $value->eta);
                        $arr[] = [
                            'tracking_no' => $value->trackingnumber,
                            'eta' => Carbon::parse($eta),
                            'registry_no' => $value->registrynumber,
                            'carrier_name' => $value->carriername,
                            'vessel_name' => $value->vesselname,
                            'warehouse' => $value->warehouse,
                            'container_no' => $value->containernumber,
                            'exchange_rate' => $value->exchangerate,
                            'point_of_discharge' => $value->pointofdischarge,
                            'remarks' => $value->remarks,
                            'user_id' => \Auth::id(),
                            'updated_by' => 0,
                        ];
                    } catch (\Exception $e) {
                        $arr[] = [
                            'tracking_no' => $value->trackingnumber,
                            'eta' => '2000-01-01',
                            'registry_no' => $value->registrynumber,
                            'carrier_name' => $value->carriername,
                            'vessel_name' => $value->vesselname,
                            'warehouse' => $value->warehouse,
                            'container_no' => $value->containernumber,
                            'exchange_rate' => $value->exchangerate,
                            'point_of_discharge' => $value->pointofdischarge,
                            'remarks' => $value->remarks,
                            'user_id' => \Auth::id(),
                            'updated_by' => 0,
                        ];
                    }
                }
                if(!empty($arr)){
                    Shipment::insert($arr);

                    Storage::delete('public/' . basename(Storage::url($csv)));

                    $message = [
                        'msgTitle' => 'Success!',
                        'msgBody' => 'The data has been successfully imported to our database.',
                        'dialogType' => 'success'
                    ];

                    return response()->json($message, 201);
                }
            }
        }
    }
}
