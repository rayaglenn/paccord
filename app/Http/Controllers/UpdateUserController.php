<?php

namespace App\Http\Controllers;

use App\User;
Use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class UpdateUserController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        if($request->validated()){
            $user = User::find($request->id);
            $user->name = $request->name;
            $user->email = $request->email;
            
            if($user->save()){
                $message = [
                    'msgTitle' => 'User Updated!',
                    'msgBody' => 'The user has been updated!',
                    'dialogType' => 'success'
                ];

                return response()->json($message, 200);
            }
        }
    }
}
