<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'required',
            // 'email' => 'required|email|unique:users',
            // 'current_password' => 'required|min:8',
            // 'password' => 'required|min:8|confirmed'
            'name' => ['required'],
            'email' => [
                'required',
                Rule::unique('users')->ignore($this->id),
            ],
            'current_password' => ['required', 'min:8'],
            'password' => ['required', 'min:8', 'confirmed']
        ];
    }
}
