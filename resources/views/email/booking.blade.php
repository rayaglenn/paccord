@component('mail::message')
# Booking Inquiry

- Shipment Type: **{{ $request->type }}**
- Name: **{{ $request->name }}**
- Email: **{{ $request->email }}**
- Contact #: **{{ $request->mobile }}**
- Subject: **{{ $request->subject }}**

{{ $request->message }}

@component('mail::button', ['url' => ''])
Open in a new tab
@endcomponent

Thanks,<br>
{{ $request->name }}
@endcomponent
