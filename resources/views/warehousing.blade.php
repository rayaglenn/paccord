@extends('templates.master')

@section('services')current
@endsection

@section('page-title')Warehousing - Pacific Concord Container Lines. Inc.
@endsection

<!-- Page specific stylesheets -->
@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ mix('css/warehousing.css') }}">
    <link rel="stylesheet" href="{{ mix('css/warehousing-mobile.css') }}">
@endsection

@section('body')
    <div class="intro-wrapper d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6">
                    <h2>Warehousing &amp; Distribution</h2>
                    <p>For storage and distribution, we make sure that your cargoes are safe and handled based on its specific needs.</p>
                    <p class="animated bounce infinite delay-2s slow"><i class="fa fal fa-chevron-down"></i></p>
                </div>
            </div>
        </div>
    </div>

    <div class="project-cargo-container">
        <div class="container">
            <div class="row">

                <div class="col-xl-4 image-columns first-image-column">
                    <a href="{{ asset('images/project-cargo-1.jpg') }}" class="image-link">
                        <img src="{{ asset('images/project-cargo-1.jpg') }}" alt="Project Cargo Image">
                    </a>

                    <a href="{{ asset('images/project-cargo-4.jpg') }}" class="image-link">
                        <img src="{{ asset('images/project-cargo-4.jpg') }}" alt="Project Cargo Image">
                    </a>
                </div>

                <div class="col-xl-4 image-columns">
                    <a href="{{ asset('images/project-cargo-2.jpg') }}" class="image-link">
                        <img src="{{ asset('images/project-cargo-2.jpg') }}" alt="Project Cargo Image">
                    </a>

                     <a href="{{ asset('images/project-cargo-5.jpg') }}" class="image-link">
                        <img src="{{ asset('images/project-cargo-5.jpg') }}" alt="Project Cargo Image">
                    </a>
                </div>

                <div class="col-xl-4 image-columns">
                    <a href="{{ asset('images/project-cargo-3.jpg') }}" class="image-link">
                        <img src="{{ asset('images/project-cargo-3.jpg') }}" alt="Project Cargo Image">
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- Page specific scripts -->
@section('page-scripts')
    <script src="{{ asset('js/magnific-popup.js') }}"></script>
    <script>
        $(function(){
            $('.image-link').magnificPopup({
                type: 'image',
                zoom: {
                    enabled: true,
                    duration: 300,
                    easing: 'ease-in-out',
                    opener: function(openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                    }
                }
            });
        });
    </script>
@endsection
