@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3 login-form-container">
                <h1>Paccord Inc. Admin Login</h1>
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf
                    <div class="form-group">
                        <i class="fas fa-envelope"></i>
                        <input type="email" placeholder="Email" name="email" class="form-control m-input{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required>
                    </div>

                    <div class="form-group">
                        <i class="fas fa-key"></i>
                        <input id="password" type="password" class="form-control  m-input m-login__form-input--last{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                    </div>

                    <div class="row">
                        {{-- <div class="col text-left">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                <span></span>
                            </label>
                        </div> --}}

                        <div class="col text-center">
                            <a href="{{ route('password.request') }}" class="forgot-password-link">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                    </div>
                    <div class="btn-container">
                        <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-xl-6 offset-xl-3 error-messages">
                @if ($errors->has('email'))
                    <span class="invalid-feedback alert alert-warning" role="alert" style="font-size: 14px; font-weight: 700">
                        {{ $errors->first('email') }}
                    </span>
                @endif

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
@endsection
