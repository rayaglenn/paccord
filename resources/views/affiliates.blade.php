@extends('templates.master')

@section('affiliates')current
@endsection

@section('page-title')Affiliates - Pacific Concord Container Lines. Inc.
@endsection

@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- {{-- <link rel="stylesheet" href="{{ asset('css/animate.css') }}"> --}} -->
    <link rel="stylesheet" href="{{ mix('css/affiliates-combined.css') }}">
    <!-- <link rel="stylesheet" href="{{ mix('css/affiliates.css') }}"> -->
    <!-- <link rel="stylesheet" href="{{ asset('css/affiliates-mobile.css') }}"> -->
@endsection

@section('body')
    <div class="intro-wrapper d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Our Affiliates</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis.</p>
                    <p class="animated bounce infinite delay-2s slow"><i class="fa fal fa-chevron-down"></i></p>
                </div>
            </div>
        </div>
    </div>

    <div class="affiliates-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xl-4 order-xl-1 affiliate">
                    <h2>Insurance</h2>
                    <ul>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Maritime Mutual Insurance Assn. (NZ) Ltd.</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-xl-4 order-xl-2 affiliate">
                    <h2>Licenses</h2>
                    <ul>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Philippine Shipper's Bureau (PSB)</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Philippine Economic Zone Authority (PEZA)</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Civil Aeronautics Boards (CAB)</a></li>
                    </ul>
                </div>

                <div class="col-md-6 order-md-6 order-lg-6 order-xl-3 col-xl-4 affiliate">
                    <h2>Global Affiliations</h2>
                    <ul>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">New!The Globalink Network</a> </li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">China Global Logistics Network (CGLN)</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-xl-4 order-xl-5 affiliate">
                    <h2>Membership</h2>
                    <ul>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Asean Freight Forwarders Association</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Philippine Exporters Confederation</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Epza Federation Of Forwarders And Truckers</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Philippine Chamber Of Commerce Inc.</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-xl-4 order-xl-4 offset-xl-2 affiliate">
                    <h2>Other Affiliates</h2>
                    <ul>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Transmodal Int’l Inc.</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Ultimate Freight, Inc.</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Idendigzy Realty Dev’t.</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">Philippine Consolidated Investors Corp.</a></li>
                        <li><i class="fa fas fa-check-circle"></i> <a href="#">PCIC and IDendigzy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
