@extends('templates.master')

@section('inquiry')current
@endsection

@section('page-title')Customer Inquiry - Pacific Concord Container Lines. Inc.
@endsection

@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('css/animate.css') }}"> --}}
    <link rel="stylesheet" href="{{ mix('css/inquiry-combined.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/inquiry-mobile.css') }}"> --}}
@endsection

@section('body')
    <div class="intro-wrapper d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xl-6">
                    <h2>We are here to help. <br>Let's Talk.</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis.</p>
                    <p class="animated bounce infinite delay-2s slow"><i class="fa fal fa-chevron-down"></i></p>
                </div>
            </div>
        </div>
    </div>

    <div class="inquiry-container">
        <div class="container">
            <div class="row">
                <div id="app" class="col-xs-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3 inquiry-form-container">
                    <img src="{{ asset('images/customer-service.svg') }}" alt="Customer Service Icon">
                    <form @keydown="booking.errors.clear()">
                        <header>
                            <h3>We're glad to talk to you.</h3>
                            <p>Use our form below to sent us an inquiry, or anything<br> you want to talk about. Or call us via our phone numbers <a href="branches">here</a>.</p>
                        </header>

                        <div class="form-group">
                            <label>Please select the type of shipment...</label>
                            <select class="custom-select" v-model="booking.type">
                                <option value="Sea">Sea</option>
                                <option value="Air">Air</option>
                                <option value="Domestic">Domestic</option>
                                <option value="Warehouse and Distribution">Warehouse and Distribution</option>
                                <option value="Project Cargo Handling">Project Cargo Handling</option>
                                <option value="Customs Clearance">Customs Clearance</option>
                                <option value="Freight Forwarding">Freight Forwarding</option>
                            </select>
                            <small class="form-text user text-danger" v-show="booking.errors.has('type')" v-text="booking.errors.get('type')"></small>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="What's your name?" v-model="booking.name">
                            <small class="form-text user text-danger" v-show="booking.errors.has('name')" v-text="booking.errors.get('name')"></small>
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="How about your email address?" v-model="booking.email">
                            <small class="form-text user text-danger" v-show="booking.errors.has('email')" v-text="booking.errors.get('email')"></small>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your mobile #" v-model="booking.mobile">
                            <small class="form-text user text-danger" v-show="booking.errors.has('mobile')" v-text="booking.errors.get('mobile')"></small>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Inquiry Subject" v-model="booking.subject">
                            <small class="form-text user text-danger" v-show="booking.errors.has('subject')" v-text="booking.errors.get('subject')"></small>
                        </div>

                        <div class="form-group">
                            <textarea rows="5" class="form-control" placeholder="Okay let's get started. How can we help you?" v-model="booking.message"></textarea>
                            <small class="form-text user text-danger" v-show="booking.errors.has('message')" v-text="booking.errors.get('message')"></small>
                        </div>

                        <div class="send-btn-container d-flex justify-content-center">
                            <button type="button" class="btn btn-success" @click="sendBooking"><i class="fab fa-telegram-plane"></i> Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/booking-app.js') }}"></script>
@endsection
