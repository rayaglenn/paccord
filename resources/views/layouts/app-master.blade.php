<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Track &amp; Trace Sytem - Pacific Concord Container Lines. Inc.</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
    <div id="app">
        <div class="header-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-3">
                        <header>
                            <h1>Track &amp; Trace | Paccord Inc.</h1>
                        </header>
                    </div>

                    <div class="col-xl-9 header-dropdown-container">
                        <router-link to="/new-shipment" tag="a" class="btn btn-success new-shipment-btn">
                            New Shipment</a>
                        </router-link>

                        <ul>
                            <li class="profile-pic-container">
                                <img src="{{ asset('storage/user.png') }}" alt="User Profile Picture">

                                <ul class="header-dropdown">
                                    <li>
                                    <router-link to="/profile/{{ \Auth::id() }}" tag="a" exact>
                                            <i class="far fa-user-circle"></i> My Profile</a>
                                        </router-link>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="fas fa-sign-out-alt"></i>  {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="body">
            <div class="container-fluid">
                <div class="row">
                    <nav class="col-xl-2 main-navigation">
                        <ul>
                            <li>
                                <router-link to="/" tag="a" class="m-menu__item" exact>
                                    <i class="fas fa-home"></i> Dashboard</a>
                                </router-link>

                            </li>
                            <li class="nav-header">Shipment</li>
                            <li>
                                <router-link to="/new-shipment" tag="a" class="m-menu__item" exact>
                                    <i class="fas fa-plus-square"></i> New Shipment</a>
                                </router-link>
                            </li>
                            <li class="nav-header">User Accounts</li>
                            <li>
                                <router-link to="/users" tag="a" class="m-menu__item" exact>
                                    <i class="fas fa-user-plus"></i> Users</a>
                                </router-link>
                            </li>
                        </ul>
                    </nav>

                    <keep-alive>
                        @yield('router-view')
                    </keep-alive>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
