@extends('templates.master')

@section('about')current
@endsection

@section('page-title')About Us - Pacific Concord Container Lines. Inc.
@endsection

@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('css/animate.css') }}"> --}}
    <link rel="stylesheet" href="{{ mix('css/about-combined.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/about-mobile.css') }}"> --}}
@endsection

@section('body')
    <div class="intro-wrapper d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h2>Our Key to Success. <br>Passionate People.</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis.</p>
                    <p class="animated bounce infinite delay-2s slow"><i class="fa fal fa-chevron-down"></i></p>
                </div>
            </div>
        </div>
    </div>

    <div class="about-container">
        <div class="container">
            <div class="row">
                <h2>Our Key Officers</h2>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-8 d-flex bd-highlight officer officer-left">
                    <div class="img-section p-2 flex-shrink-2 bd-highlight purple">
                        <img src="{{ asset('images/employee1.png') }}" alt="Employee">
                    </div>
                    <div class="texts-section p-2 w-100 bd-highlight">
                        <header>
                            <h3>Barbie B. Rivadeneira</h3>
                            <p>President</p>
                        </header>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 offset-md-4 d-flex bd-highlight officer officer-right">
                    <div class="texts-section p-2 w-100 bd-highlight">
                        <header>
                            <h3>Ferdie A. Espeleta</h3>
                            <p>Sales & Marketing Manager</p>
                        </header>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    </div>

                    <div class="img-section p-2 flex-shrink-2 bd-highlight miaka">
                        <img src="{{ asset('images/employee2.png') }}" alt="Employee">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 d-flex bd-highlight officer officer-left">
                    <div class="img-section p-2 flex-shrink-2 bd-highlight ice">
                        <img src="{{ asset('images/employee1.png') }}" alt="Employee">
                    </div>
                    <div class="texts-section p-2 w-100 bd-highlight">
                        <header>
                            <h3>Cleofe M. Montes</h3>
                            <p>Forwarding Manager (OIC)</p>
                        </header>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 offset-md-4 d-flex bd-highlight officer officer-right">
                    <div class="texts-section p-2 w-100 bd-highlight">
                        <header>
                            <h3>Catherine E. Sandigan</h3>
                            <p>Accounting Manager</p>
                        </header>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    </div>

                    <div class="img-section p-2 flex-shrink-2 bd-highlight happiness">
                        <img src="{{ asset('images/employee2.png') }}" alt="Employee">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 d-flex bd-highlight officer officer-left">
                    <div class="img-section p-2 flex-shrink-2 bd-highlight royal">
                        <img src="{{ asset('images/employee1.png') }}" alt="Employee">
                    </div>
                    <div class="texts-section p-2 w-100 bd-highlight">
                        <header>
                            <h3>Arlene T. Gunhuran </h3>
                            <p>Branch Manager (Southern Phils)</p>
                        </header>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 offset-md-4 d-flex bd-highlight officer officer-right">
                    <div class="texts-section p-2 w-100 bd-highlight">
                        <header>
                            <h3>Garnette L. Dario</h3>
                            <p>HR & Admin Manager</p>
                        </header>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    </div>

                    <div class="img-section p-2 flex-shrink-2 bd-highlight pink">
                        <img src="{{ asset('images/employee2.png') }}" alt="Employee">
                    </div>
                </div>
            </div>

            <div class="row about-bottom-info">
                <div class="col-xs-12 col-md-12 col-lg-12 col-xl-7 image-holder">
                    <img src="{{ asset('images/about-bottom-img.png') }}" alt="Warehouse Image">
                </div>

                <div class="col-xs-12 col-md-12 col-lg-12 col-xl-5 text-holder">
                    <header>
                        <h2>About Our Company</h2>
                    </header>

                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>

                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
