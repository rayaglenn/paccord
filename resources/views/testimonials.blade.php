@extends('templates.master')

@section('testimonials')current
@endsection

@section('page-title')Testimonials - What Does Our Happy Customers Say About Us
@endsection

@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ mix('css/testimonials.css') }}">
    <link rel="stylesheet" href="{{ mix('css/testimonials-mobile.css') }}">
@endsection

@section('body')
    <div class="intro-wrapper d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Testimonials</h2>
                    <p>Our happy clients have something to say about us and our services.</p>
                    <p class="animated bounce infinite delay-2s slow"><i class="fa fal fa-chevron-down"></i></p>
                </div>
            </div>
        </div>
    </div>

    <div class="testimonials-container">
        <div class="container">
            <div class="row">

                <!-- Column 1 -->
                <div class="col-xl-4 first-column testimonial-wrapper">
                    <blockquote class="testimonial grid-item">
                        <p>As a leading distributor and system integrator, 80% of our products are imported, thus having a reliable partner in facilitating our importation, brokerage, and trucking services is a crucial area of our business. Since 2001 Pacific Concord with exemplary services; made us focus in growing our business.</p>

                        <p class="person">by Ms. Judith</p>
                    </blockquote>
                </div>
                <!-- End of Column 1 -->

                <!-- Column 2 -->
                <div class="col-xl-4 second-column testimonial-wrapper">
                    <blockquote class="testimonial grid-item">
                        <p>Congrats to us, we achieved 100% performance rating last August. I hope this will stay at this level. Thank you for all our efforts.</p>

                        <p class="person">by SERIAL</p>
                    </blockquote>
                </div>
                <!-- End of Column 2 -->

                <!-- Column 3 -->
                <div class="col-xl-4 third-column testimonial-wrapper">
                    <blockquote class="testimonial grid-item">
                        <p>From 275k to 270k usd cost per shipment, Paccord helps our revenue increased by saving at least 15% per shipment. This is on top of their great service in releasing our importations from port and delivering all our shipments safely at our warehouse.</p>

                        <p>They maintain good customer services by keeping us up to date on shipment status.</p>

                        <p>With this, we are glad to have find a good partner in business, that ensures our shipment arrives safely to its destination while enjoying practical rates.</p>

                        <p class="person">by Cris Molina of JVF Commercial and Project Development Services</p>
                    </blockquote>
                </div>
                <!-- End of Column 3 -->

            </div>
        </div>
    </div>
@endsection
