@extends('templates.master')

@section('track-trace')current
@endsection

@section('page-title')Track &amp; Trace - Pacific Concord Container Lines. Inc.
@endsection

@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('css/animate.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/tracking.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/tracking-mobile.css') }}"> --}}
    <link rel="stylesheet" href="{{ mix('css/tracking-combined.css') }}">
@endsection

@section('body')
    <div id="app">
        <div class="intro-wrapper d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h2>Track &amp; Trace</h2>
                        <p>Find your shipment with ease. Use our real-time shipment tracking below.</p>
                        <input type="text" class="form-control" placeholder="Enter your tracking # then press 'Track' button below." v-model="shipment.tracking_no">
                        <div class="tracking-btn-container d-flex justify-content-center">
                            <button type="button" class="btn btn-danger" @click="trackShipment"><i class="fab fa-slack-hash"></i> Track</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="tracking-container">
            <div class="container">
                {{-- <div class="row" v-if="shipment_info.msgBody">
                    <div class="col-xl-8 offset-xl-2 no-record">
                        <img src="{{ asset('images/no-records.svg') }}" alt="No Records Image">
                    </div>
                </div> --}}
                <div class="row">
                    <header class="col-xl-12">
                        <h2><span>Tracking #</span> @{{ shipment_info.tracking_no }}</h2>
                    </header>

                    <div class="col-xl-4 details">
                        <h3>Estimated Time of Arrival</h3>
                        <p>@{{ shipment_info.eta }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Registry Number</h3>
                        <p>@{{ shipment_info.registry_no ? shipment_info.registry_no : '--' }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Carrier Name</h3>
                        <p>@{{ shipment_info.carrier_name ? shipment_info.carrier_name : '--' }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Vessel Name</h3>
                        <p>@{{ shipment_info.vessel_name ? shipment_info.vessel_name : '--' }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Warehouse</h3>
                        <p>@{{ shipment_info.warehouse ? shipment_info.warehouse : '--' }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Container Number</h3>
                        <p>@{{ shipment_info.container_no ? shipment_info.container_no : '--'  }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Exchange Rate</h3>
                        <p>@{{ shipment_info.exchange_rate ? shipment_info.exchange_rate : '--'  }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Point of Discharge</h3>
                        <p>@{{ shipment_info.point_of_discharge ? shipment_info.point_of_discharge : '--' }}</p>
                    </div>
                    <div class="col-xl-4 details">
                        <h3>Remarks</h3>
                        <p>@{{ shipment_info.remarks ? shipment_info.remarks : '--' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- Page specific scripts -->
@section('page-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/tracking-app.js') }}"></script>
@endsection
