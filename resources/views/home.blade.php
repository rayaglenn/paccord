@extends('layouts.app-master')

@section('router-view')
    <transition name="router-anim">
        <router-view :key="$route.path"></router-view>
    </transition>
@endsection
