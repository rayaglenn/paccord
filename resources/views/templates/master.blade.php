<!--
=======================================================

    Proudly hand coded by Bizwex!

    This is not a template or wordpress
    cheap theme dude! it's entirely
    designed by us. Just saying.

    "It's better to create a single, high-quality
    beautiful product that a thousand people
    will love, rather than create many
    mediocre products that a thousand
    people will hate."

                                    - Your's truly

    https://www.bizwex.com

=======================================================
-->
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('page-title')</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- Global Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ mix('css/global.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/hamburgers.min.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/global-mobile.css') }}"> --}}
    <!-- Page Specific Styles -->
    @yield('page-css')

</head>
<body>
    <button class="hamburger hamburger--collapse" type="button">
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>
    </button>

    <a href="/" title="Pacific Concord Container Lines. Inc." class="mobile-header-logo">
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 573.92 529">
            <g>
                <polygon class="cls-1" points="259.51 0 259.51 149.72 89.83 149.72 89.83 529 0 529 0 0 259.51 0"/>
                <polygon class="cls-1" points="187.75 253.57 259.51 253.57 259.51 340.91 219.59 340.91 219.59 373.34 259.51 373.34 259.51 425.45 187.75 425.45 187.75 253.57"/>
                <polygon class="cls-1" points="259.51 184.65 259.51 221.13 154.71 221.13 154.71 457.88 259.51 457.88 259.51 529 122.27 529 122.27 184.65 259.51 184.65"/>
                <rect class="cls-1" x="419.21" width="154.71" height="94.82"/>
                <rect class="cls-1" x="419.21" y="129.75" width="154.71" height="219.58"/>
                <polygon class="cls-1" points="573.91 384.27 573.91 529 294.44 529 294.44 0 384.27 0 384.27 384.27 573.91 384.27"/>
            </g>
        </svg>
    </a>

    <div class="navigation-wrapper">
        <div class="container-fluid">
            <div class="row">
                <!-- Logo Container -->
                <div class="col-md-6 col-lg-12 col-xl-3 logo-container d-flex align-items-center">
                    <a href="/" title="Pacific Concord Container Lines. Inc.">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 573.92 529">
                            <g>
                                <polygon class="cls-1" points="259.51 0 259.51 149.72 89.83 149.72 89.83 529 0 529 0 0 259.51 0"/>
                                <polygon class="cls-1" points="187.75 253.57 259.51 253.57 259.51 340.91 219.59 340.91 219.59 373.34 259.51 373.34 259.51 425.45 187.75 425.45 187.75 253.57"/>
                                <polygon class="cls-1" points="259.51 184.65 259.51 221.13 154.71 221.13 154.71 457.88 259.51 457.88 259.51 529 122.27 529 122.27 184.65 259.51 184.65"/>
                                <rect class="cls-1" x="419.21" width="154.71" height="94.82"/>
                                <rect class="cls-1" x="419.21" y="129.75" width="154.71" height="219.58"/>
                                <polygon class="cls-1" points="573.91 384.27 573.91 529 294.44 529 294.44 0 384.27 0 384.27 384.27 573.91 384.27"/>
                            </g>
                        </svg>
                    </a>

                    <h1><a href="/" title="Pacific Concord Container Lines. Inc.">Paccord Inc.</a></h1>
                </div>

                <!-- Main Navigation -->
                <nav class="col-md-12 col-lg-12 col-xl-9">
                    <ul>
                        <li class="home"><a href="/" class="@yield('home')" title="Paccord Homepage">Home</a></li>
                        <li class="services"><a href="services" class="@yield('services')" title="Paccord Services">Services</a></li>
                        <li class="affiliates"><a href="affiliates" class="@yield('affiliates')" title="Paccord Affiliates">Affiliates</a></li>
                        <li class="branches"><a href="branches" class="@yield('branches')" title="Paccord Branches">Branches</a></li>
                        <li class="about"><a href="about" class="@yield('about')" title="About Our Company">About Us</a></li>
                        <li class="inquiry"><a href="inquiry" class="@yield('inquiry')" title="Inquire to us">Booking</a></li>
                        <li class="track-trace"><a href="tracking" class="@yield('track-trace')" title="Track your packages">Track & Trace</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    @yield('body')

    <!-- Footer -->
    <div class="footer-container">
        <div class="container-fluid">
            <footer class="row">
                <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4 col-xl-4 offset-xl-0 d-flex align-items-center">
                     <a href="/" title="Pacific Concord Container Lines. Inc." class="footer-logo-link">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 573.92 529">
                            <g>
                                <polygon class="cls-1" points="259.51 0 259.51 149.72 89.83 149.72 89.83 529 0 529 0 0 259.51 0"/>
                                <polygon class="cls-1" points="187.75 253.57 259.51 253.57 259.51 340.91 219.59 340.91 219.59 373.34 259.51 373.34 259.51 425.45 187.75 425.45 187.75 253.57"/>
                                <polygon class="cls-1" points="259.51 184.65 259.51 221.13 154.71 221.13 154.71 457.88 259.51 457.88 259.51 529 122.27 529 122.27 184.65 259.51 184.65"/>
                                <rect class="cls-1" x="419.21" width="154.71" height="94.82"/>
                                <rect class="cls-1" x="419.21" y="129.75" width="154.71" height="219.58"/>
                                <polygon class="cls-1" points="573.91 384.27 573.91 529 294.44 529 294.44 0 384.27 0 384.27 384.27 573.91 384.27"/>
                            </g>
                        </svg>

                    </a>
                    <h4><a href="/" title="Pacific Concord Container Lines. Inc.">Paccord Inc.</a> <br><span>Copyright {{ now()->year }} &copy; | All Rights Reserved.</span> </h4>
                </div>
                <div class="col-md-12 col-lg-12 col-xl-8 footer-nav">
                    <ul>
                        <li class="home"><a href="/" class="@yield('home')" title="Paccord Homepage">Home</a></li>
                        <li class="services"><a href="services" class="@yield('services')" title="Paccord Services">Services</a></li>
                        <li class="affiliates"><a href="affiliates" title="Paccord Affiliates">Affiliates</a></li>
                        <li class="branches"><a href="branches" title="Paccord Branches">Branches</a></li>
                        <li class="about"><a href="about" title="About Our Company">About Us</a></li>
                        <li class="inquiry"><a href="inquiry" title="Inquire to us">Inquiry</a></li>
                        <li class="booking"><a href="#" title="Online Booking">Booking</a></li>
                        <li class="track-trace"><a href="#" title="Track your packages">Track & Trace</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <p>3rd Floor TMI Centre Bldg. Arzobispo St. Intramuros, Manila, Philippines</p>
                </div>
            </footer>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ asset('js/menu-script.js') }}"></script>
    {{-- Page specific scripts --}}
    @yield('page-scripts')
</body>
</html>
