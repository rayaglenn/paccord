@extends('templates.master')

@section('branches')current
@endsection

@section('page-title')Branches - Pacific Concord Container Lines. Inc.
@endsection

@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('css/animate.css') }}"> --}}
    <link rel="stylesheet" href="{{ mix('css/branches-combined.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/branches-mobile.css') }}"> --}}
@endsection

@section('body')
    <div class="intro-wrapper d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6">
                    <h2>Our Area of Operations</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis.</p>
                    <p class="animated bounce infinite delay-2s slow"><i class="fa fal fa-chevron-down"></i></p>
                </div>
            </div>
        </div>
    </div>

    <div class="branches-container">
        <div class="container">
            <div class="row">
                <div class="order-1 order-xs-2 order-xl-1 col-lg-4 order-md-2 col-md-6 branch">
                    <div>
                        <h2>Cebu Branch</h2>
                        <p>R & R Building Ground Floor, A.C. Cortes Avenue, Alang-Alang  Mandaue City Cebu Philippines</p>
                        <ul>
                            <li>Tel. Nos. (63 32) 345-0936/ 344-9698</li>
                            <li>Fax No. (63 32) 344-7826</li>
                            <li>Email : <a href="mailto:cebu@paccord.com">cebu@paccord.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="order-xs-1 order-xl-2 col-md-6 order-md-1 col-lg-4 main">
                    <div>
                        <h2>Main Office</h2>
                        <p>3rd Floor TMI Centre Bldg., Arzobispo St. Intramuros, Manila, Philippines </p>
                        <ul>
                            <li>Tel. Nos.: (63 02) 522-8000 / 536-2330</li>
                            <li>Fax. Nos (63 02) 526-2072 / 256-1987-88 </li>
                            <li>Email : <a href="mailto:sales@paccord.com">sales@paccord.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="order-3 order-xs-3 order-md-3 col-md-6 col-lg-4 branch">
                    <div>
                        <h2>Cavite Branch</h2>
                        <p>Room 202, Linaheim Building, 1st Street, Phase 1 Cavite Export Processing Zone, Rosario Cavite 4106, Philippines</p>
                        <ul>
                            <li>Tel. No. (63 46) 437-1435</li>
                            <li>Fax No. (63 46) 971-0481</li>
                        </ul>
                    </div>
                </div>

                <div class="order-xs-4 order-md-4 col-md-6 col-lg-4 branch">
                    <div>
                        <h2>Davao Branch</h2>
                        <p>2D, D3G Y10 Bldg., Km. 5 Buhangin Road, Davao City Philippines</p>
                        <ul>
                            <li>Tel. No. (63 82) 235 8936</li>
                            <li>Email : <a href="mailto:pcclidavao@paccord.com">pcclidavao@paccord.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="order-xs-5 order-md-5 col-md-6 col-lg-4 branch">
                    <div>
                        <h2>Zamboanga Branch</h2>
                        <p>Room 2 2nd Floor Radja Building, Gov. Camins Avenue, Zamboanga City 7000, Philippines </p>
                        <ul>
                            <li>TeleFax No. (63 62) 992-2770</li>
                            <li>Email: <a href="mailto:pcclizamboanga@paccord.com">pcclizamboanga@paccord.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="order-xs-6 order-md-6 col-md-6 col-lg-4 branch">
                    <div>
                        <h2>General Santos Branch</h2>
                        <p>2F, Sarangani Zegen Corp. Building (former Verez Bldg.) Crossing Makar, brgy. Labangal, General Santos City Philippines</p>
                        <ul>
                            <li>Tel. No. (63 83) 552-3167</li>
                            <li>Email : <a href="mailto:pccligensan@paccord.com">pccligensan@paccord.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="order-xs-7 order-md-7 col-md-6 col-lg-4 branch">
                    <div>
                        <h2>Cagayan De Oro Branch</h2>
                        <p>Suite 102, Cartalitz Bldg., Cayetano Pacana St.,Cagayan de oro City, Misamis Oriental</p>
                        <ul>
                            <li>Tel. No. (63 88) 231-5854</li>
                            <li>Email : <a href="mailto:pcclicagayan@paccord.com">pcclicagayan@paccord.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="order-xs-7 order-md-7 col-md-6 col-lg-4 branch">
                    <div>
                        <h2>Laguna</h2>
                        <p>122, E Science Ave., Biñan Laguna</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
