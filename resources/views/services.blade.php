@extends('templates.master')

@section('services')current
@endsection

@section('page-title')Services - Pacific Concord Container Lines. Inc.
@endsection

@section('page-css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('css/animate.css') }}"> --}}
    <link rel="stylesheet" href="{{ mix('css/services-combined.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/services-mobile.css') }}"> --}}
@endsection

@section('body')
    <div class="intro-wrapper d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>We Deliver Excellent Services</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis.</p>
                    <p class="animated bounce infinite delay-2s slow"><i class="fa fal fa-chevron-down"></i></p>
                </div>
            </div>
        </div>
    </div>

    <div class="services-container">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-6 d-flex align-items-center">
                    <img src="{{ asset('images/cargo-ship-svg.svg') }}" alt="Cargo Ship" class="cargo-ship">
                </div>
                <section class="col-md-7 col-lg-5 offset-lg-1">
                    <h2>Inbound & Outbound FCL <br>& Cargo Consolidation</h2>
                    <p>Our Company offers Outbound and Inbound Seafreight & Airfreight Consolidation to from all parts of the world. We utilize Service Contracts which gives us the ability to work with the Shipping Lines that service different routings worldwide.</p>

                    <p>As "Your Total Transport Partner" , our networks of Agents are experienced in all aspects of shipping and together we can offer complete package to our clients for all kinds of cargoes and all modes of tranport.</p>

                    <p>And while your cargoes are on transit, rest assured that your cargoes are well monitored by our staff and we will make sure that you will be informed of the status of your shipments from time to time until it reaches its final destination. </p>
                </section>
            </div>

            <section class="row services-list">
                <header>
                    <h2>What Do We Offer</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis.</p>
                </header>

                <div class="col-md-6 col-lg-4 service">
                    <img src="{{ asset('images/trucks-thumbnail.jpg') }}" alt="Trucks Thumbnail" class="rounded-circle">
                    <h3>Trucking And Haulage</h3>
                    <p>Our services included pick-up, hauling and trucking of your cargoes. With a team of professional ans skilled truck drivers, we are confident the your cargoes will be delivered to our warehouse safely and on time.</p>
                </div>
                <div class="col-md-6 col-lg-4 service">
                    <img src="{{ asset('images/packing-thumbnail.jpg') }}" alt="Packing Thumbnail" class="rounded-circle">
                    <h3>Warehousing and Distribution</h3>
                    <p>For storage and distribution, we make sure that your cargoes are safe and handled based on its specific needs.</p>
                </div>
                <div class="col-md-6 col-lg-4 service">
                    <img src="{{ asset('images/warehousing-thumbnail.jpg') }}" alt="Warehouse Thumbnail" class="rounded-circle">
                    <h3>Packing and Crating</h3>
                    <p>We are skilled in handling and overweight cargoes. No matter what your cargoe is, we customize the packaging and crating of your cargoes and provide proper markings and labelling to fit your specific requirments. </p>
                </div>
                <div class="col-md-6 col-lg-4 service">
                    <img src="{{ asset('images/customs-thumbnail.jpg') }}" alt="Customs Thumbnail" class="rounded-circle">
                    <h3>Customs Clearance</h3>
                    <p>With the help of our assigned Customs Clearance Broker Networks, your goods are cleared in an effifient and timely manner and will be classified accurately and ensure that they comply with specific product requirements and avoid any delays in Customs.</p>
                </div>
                <div class="col-md-6 col-lg-4 service">
                    <img src="{{ asset('images/cargo-handling-thumbnail.jpg') }}" alt="Cargo Handling Thumbnail" class="rounded-circle">
                    <h3>Project Cargo Handling</h3>
                    <p>For Heavy Cargo Movements, we make sure that every detail is carefully studied and analyzed, from the commodity, size, weight and the equipments needed to knowthe specific handling requirments of your shipments. </p>
                </div>
                <div class="col-md-6 col-lg-4 service">
                    <img src="{{ asset('images/delivery-thumbnail.jpg') }}" alt="Delivery Thumbnail" class="rounded-circle">
                    <h3>Door to Door Delivery</h3>
                    <p>We also specialized in moving your cargoes like furniture, personal effects and other household items to and from your door to any parts of the world at a very reasonable price. We evaluate your cargoes and prepared all the necessary requirments needed for shipping and we will closely monitor your shipment and update you of the status of your cargo from time to time.</p>
                </div>
            </section>
        </div>
    </div>
@endsection
