
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
import Vue from 'vue';
import Form from './utils/Form';
import moment from 'moment';
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);
window.axios = require('axios');
window.axios = axios;
window.Form = Form;

window.Vue = require('vue');

const app = new Vue({
    el: '#app',

    data(){
        return {
            booking: new Form({
                type: '',
                name: '',
                email: '',
                mobile: '',
                subject: '',
                message: ''

            })
        }
    },

    methods: {

        /**
         * Track shipment by tracking #.
         */
        sendBooking(){
            let self = this;
            self.booking
                .post('booking')
                .then(function(response){
                    Vue.swal(response.msgTitle, response.msgBody, response.dialogType);
                });
        }
    }
});
