import VueRouter from 'vue-router';

let routes = [
    {
        path: "/",
        name: "Home",
        component: require("./views/Home.vue"),
    },

    {
        path: "/new-shipment",
        name: "NewShipment",
        component: require("./views/NewShipment.vue"),
    },

    {
        path: "/shipment/:id",
        name: "shipment",
        component: require("./views/Shipment.vue"),
    },

    {
        path: "/profile/:id",
        name: "Profile",
        component: require("./views/Profile.vue"),
    },

    {
        path: "/users",
        name: "Users",
        component: require("./views/Users.vue"),
    },

    {
        path: "/unauthorized/",
        name: "unauthorized",
        component: require("./views/Unauthorized")
    }
];

export default new VueRouter({
    routes,
    linkActiveClass: 'nav-active'
});