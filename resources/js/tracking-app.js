
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
import Vue from 'vue';
import Form from './utils/Form';
import moment from 'moment';
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);
window.axios = require('axios');
window.axios = axios;
window.Form = Form;

window.Vue = require('vue');

const app = new Vue({
    el: '#app',

    data(){
        return {
            shipment: new Form({
                tracking_no: '',
            }),
            shipment_info: {},
        }
    },

    methods: {

        /**
         * Track shipment by tracking #.
         */
        trackShipment(){
            let self = this;
            self.shipment
                .post('track-shipment')
                .then(response => {
                    response.eta = moment(response.eta).format('LL');
                    self.shipment_info = response;

                    if(self.shipment_info.msgBody){
                        Vue.swal('No Record Found', `We didn't find any shipment with that tracking #, please try again later.`, 'warning');
                    }
                });
        }
    }
});
