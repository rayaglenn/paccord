<?php

// Authentication Routes
// =============================================================
Auth::routes();
Route::get('/app', 'HomeController@index')->name('app');

// App Routes
// =============================================================
Route::middleware(['auth'])->group(function () {
    // Import CSV Shipment File.
    Route::post('import-shipments', 'ImportShipmentController@store');

    // Export shipment to excel.
    Route::get('download-file/{type}', 'ExportShipment@downloadFile');

    // BREAD shipment resourceful route.
    Route::resource('shipments', 'ShipmentController');

    Route::resource('user', 'UserController');

    Route::post('update-user/{id}', 'UpdateUserController@update');
});

// Track shipment via website tracking page.
// =============================================================
Route::post('track-shipment', 'ShipmentController@show');

// Send booking email.
// =============================================================
Route::post('booking', 'BookingController@sendBooking');

// Website Routes
// =============================================================
Route::get('/', function () {
    return view('welcome');
});

Route::get('services', function () {
    return view('services');
});

Route::get('affiliates', function () {
    return view('affiliates');
});

Route::get('branches', function () {
    return view('branches');
});

Route::get('inquiry', function () {
    return view('inquiry');
});

Route::get('about', function () {
    return view('about');
});

Route::get('tracking', function () {
    return view('tracking');
});

Route::get('testimonials', function () {
    return view('testimonials');
});

Route::get('project-cargo-handling', function () {
    return view('project-cargo');
});

Route::get('warehousing', function () {
    return view('warehousing');
});
