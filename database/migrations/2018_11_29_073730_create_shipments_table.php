<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracking_no')->unique();
            $table->date('eta');
            $table->string('registry_no')->nullable();
            $table->string('carrier_name')->nullable();
            $table->string('vessel_name')->nullable();
            $table->string('warehouse')->nullable();
            $table->string('container_no')->nullable();
            $table->string('exchange_rate')->nullable();
            $table->string('point_of_discharge')->nullable();
            $table->string('remarks')->nullable();
            $table->integer('user_id');
            $table->integer('updated_by');
            $table->tinyInteger('delivered')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
