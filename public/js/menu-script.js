$(function(){
    $(document).on('click', '.hamburger', function () {
        $(this).addClass('is-active');
        $('.navigation-wrapper').stop(0,0).animate({
            right: '0%'
        })
    });

    $(document).on('click', '.is-active', function () {
        $(this).removeClass('is-active');
        $('.navigation-wrapper').stop(0,0).animate({
            right: '-100%'
        })
    });
});