/**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */
!function (a, b, c, d) { function e(b, c) { this.settings = null, this.options = a.extend({}, e.Defaults, c), this.$element = a(b), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = { time: null, target: null, pointer: null, stage: { start: null, current: null }, direction: null }, this._states = { current: {}, tags: { initializing: ["busy"], animating: ["busy"], dragging: ["interacting"] } }, a.each(["onResize", "onThrottledResize"], a.proxy(function (b, c) { this._handlers[c] = a.proxy(this[c], this) }, this)), a.each(e.Plugins, a.proxy(function (a, b) { this._plugins[a.charAt(0).toLowerCase() + a.slice(1)] = new b(this) }, this)), a.each(e.Workers, a.proxy(function (b, c) { this._pipe.push({ filter: c.filter, run: a.proxy(c.run, this) }) }, this)), this.setup(), this.initialize() } e.Defaults = { items: 3, loop: !1, center: !1, rewind: !1, checkVisibility: !0, mouseDrag: !0, touchDrag: !0, pullDrag: !0, freeDrag: !1, margin: 0, stagePadding: 0, merge: !1, mergeFit: !0, autoWidth: !1, startPosition: 0, rtl: !1, smartSpeed: 250, fluidSpeed: !1, dragEndSpeed: !1, responsive: {}, responsiveRefreshRate: 200, responsiveBaseElement: b, fallbackEasing: "swing", slideTransition: "", info: !1, nestedItemSelector: !1, itemElement: "div", stageElement: "div", refreshClass: "owl-refresh", loadedClass: "owl-loaded", loadingClass: "owl-loading", rtlClass: "owl-rtl", responsiveClass: "owl-responsive", dragClass: "owl-drag", itemClass: "owl-item", stageClass: "owl-stage", stageOuterClass: "owl-stage-outer", grabClass: "owl-grab" }, e.Width = { Default: "default", Inner: "inner", Outer: "outer" }, e.Type = { Event: "event", State: "state" }, e.Plugins = {}, e.Workers = [{ filter: ["width", "settings"], run: function () { this._width = this.$element.width() } }, { filter: ["width", "items", "settings"], run: function (a) { a.current = this._items && this._items[this.relative(this._current)] } }, { filter: ["items", "settings"], run: function () { this.$stage.children(".cloned").remove() } }, { filter: ["width", "items", "settings"], run: function (a) { var b = this.settings.margin || "", c = !this.settings.autoWidth, d = this.settings.rtl, e = { width: "auto", "margin-left": d ? b : "", "margin-right": d ? "" : b }; !c && this.$stage.children().css(e), a.css = e } }, { filter: ["width", "items", "settings"], run: function (a) { var b = (this.width() / this.settings.items).toFixed(3) - this.settings.margin, c = null, d = this._items.length, e = !this.settings.autoWidth, f = []; for (a.items = { merge: !1, width: b }; d--;)c = this._mergers[d], c = this.settings.mergeFit && Math.min(c, this.settings.items) || c, a.items.merge = c > 1 || a.items.merge, f[d] = e ? b * c : this._items[d].width(); this._widths = f } }, { filter: ["items", "settings"], run: function () { var b = [], c = this._items, d = this.settings, e = Math.max(2 * d.items, 4), f = 2 * Math.ceil(c.length / 2), g = d.loop && c.length ? d.rewind ? e : Math.max(e, f) : 0, h = "", i = ""; for (g /= 2; g > 0;)b.push(this.normalize(b.length / 2, !0)), h += c[b[b.length - 1]][0].outerHTML, b.push(this.normalize(c.length - 1 - (b.length - 1) / 2, !0)), i = c[b[b.length - 1]][0].outerHTML + i, g -= 1; this._clones = b, a(h).addClass("cloned").appendTo(this.$stage), a(i).addClass("cloned").prependTo(this.$stage) } }, { filter: ["width", "items", "settings"], run: function () { for (var a = this.settings.rtl ? 1 : -1, b = this._clones.length + this._items.length, c = -1, d = 0, e = 0, f = []; ++c < b;)d = f[c - 1] || 0, e = this._widths[this.relative(c)] + this.settings.margin, f.push(d + e * a); this._coordinates = f } }, { filter: ["width", "items", "settings"], run: function () { var a = this.settings.stagePadding, b = this._coordinates, c = { width: Math.ceil(Math.abs(b[b.length - 1])) + 2 * a, "padding-left": a || "", "padding-right": a || "" }; this.$stage.css(c) } }, { filter: ["width", "items", "settings"], run: function (a) { var b = this._coordinates.length, c = !this.settings.autoWidth, d = this.$stage.children(); if (c && a.items.merge) for (; b--;)a.css.width = this._widths[this.relative(b)], d.eq(b).css(a.css); else c && (a.css.width = a.items.width, d.css(a.css)) } }, { filter: ["items"], run: function () { this._coordinates.length < 1 && this.$stage.removeAttr("style") } }, { filter: ["width", "items", "settings"], run: function (a) { a.current = a.current ? this.$stage.children().index(a.current) : 0, a.current = Math.max(this.minimum(), Math.min(this.maximum(), a.current)), this.reset(a.current) } }, { filter: ["position"], run: function () { this.animate(this.coordinates(this._current)) } }, { filter: ["width", "position", "items", "settings"], run: function () { var a, b, c, d, e = this.settings.rtl ? 1 : -1, f = 2 * this.settings.stagePadding, g = this.coordinates(this.current()) + f, h = g + this.width() * e, i = []; for (c = 0, d = this._coordinates.length; c < d; c++)a = this._coordinates[c - 1] || 0, b = Math.abs(this._coordinates[c]) + f * e, (this.op(a, "<=", g) && this.op(a, ">", h) || this.op(b, "<", g) && this.op(b, ">", h)) && i.push(c); this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + i.join("), :eq(") + ")").addClass("active"), this.$stage.children(".center").removeClass("center"), this.settings.center && this.$stage.children().eq(this.current()).addClass("center") } }], e.prototype.initializeStage = function () { this.$stage = this.$element.find("." + this.settings.stageClass), this.$stage.length || (this.$element.addClass(this.options.loadingClass), this.$stage = a("<" + this.settings.stageElement + ">", { class: this.settings.stageClass }).wrap(a("<div/>", { class: this.settings.stageOuterClass })), this.$element.append(this.$stage.parent())) }, e.prototype.initializeItems = function () { var b = this.$element.find(".owl-item"); if (b.length) return this._items = b.get().map(function (b) { return a(b) }), this._mergers = this._items.map(function () { return 1 }), void this.refresh(); this.replace(this.$element.children().not(this.$stage.parent())), this.isVisible() ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass) }, e.prototype.initialize = function () { if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) { var a, b, c; a = this.$element.find("img"), b = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : d, c = this.$element.children(b).width(), a.length && c <= 0 && this.preloadAutoWidthImages(a) } this.initializeStage(), this.initializeItems(), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized") }, e.prototype.isVisible = function () { return !this.settings.checkVisibility || this.$element.is(":visible") }, e.prototype.setup = function () { var b = this.viewport(), c = this.options.responsive, d = -1, e = null; c ? (a.each(c, function (a) { a <= b && a > d && (d = Number(a)) }), e = a.extend({}, this.options, c[d]), "function" == typeof e.stagePadding && (e.stagePadding = e.stagePadding()), delete e.responsive, e.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + d))) : e = a.extend({}, this.options), this.trigger("change", { property: { name: "settings", value: e } }), this._breakpoint = d, this.settings = e, this.invalidate("settings"), this.trigger("changed", { property: { name: "settings", value: this.settings } }) }, e.prototype.optionsLogic = function () { this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1) }, e.prototype.prepare = function (b) { var c = this.trigger("prepare", { content: b }); return c.data || (c.data = a("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(b)), this.trigger("prepared", { content: c.data }), c.data }, e.prototype.update = function () { for (var b = 0, c = this._pipe.length, d = a.proxy(function (a) { return this[a] }, this._invalidated), e = {}; b < c;)(this._invalidated.all || a.grep(this._pipe[b].filter, d).length > 0) && this._pipe[b].run(e), b++; this._invalidated = {}, !this.is("valid") && this.enter("valid") }, e.prototype.width = function (a) { switch (a = a || e.Width.Default) { case e.Width.Inner: case e.Width.Outer: return this._width; default: return this._width - 2 * this.settings.stagePadding + this.settings.margin } }, e.prototype.refresh = function () { this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed") }, e.prototype.onThrottledResize = function () { b.clearTimeout(this.resizeTimer), this.resizeTimer = b.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate) }, e.prototype.onResize = function () { return !!this._items.length && (this._width !== this.$element.width() && (!!this.isVisible() && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized"))))) }, e.prototype.registerEventHandlers = function () { a.support.transition && this.$stage.on(a.support.transition.end + ".owl.core", a.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(b, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () { return !1 })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", a.proxy(this.onDragEnd, this))) }, e.prototype.onDragStart = function (b) { var d = null; 3 !== b.which && (a.support.transform ? (d = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), d = { x: d[16 === d.length ? 12 : 4], y: d[16 === d.length ? 13 : 5] }) : (d = this.$stage.position(), d = { x: this.settings.rtl ? d.left + this.$stage.width() - this.width() + this.settings.margin : d.left, y: d.top }), this.is("animating") && (a.support.transform ? this.animate(d.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === b.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = a(b.target), this._drag.stage.start = d, this._drag.stage.current = d, this._drag.pointer = this.pointer(b), a(c).on("mouseup.owl.core touchend.owl.core", a.proxy(this.onDragEnd, this)), a(c).one("mousemove.owl.core touchmove.owl.core", a.proxy(function (b) { var d = this.difference(this._drag.pointer, this.pointer(b)); a(c).on("mousemove.owl.core touchmove.owl.core", a.proxy(this.onDragMove, this)), Math.abs(d.x) < Math.abs(d.y) && this.is("valid") || (b.preventDefault(), this.enter("dragging"), this.trigger("drag")) }, this))) }, e.prototype.onDragMove = function (a) { var b = null, c = null, d = null, e = this.difference(this._drag.pointer, this.pointer(a)), f = this.difference(this._drag.stage.start, e); this.is("dragging") && (a.preventDefault(), this.settings.loop ? (b = this.coordinates(this.minimum()), c = this.coordinates(this.maximum() + 1) - b, f.x = ((f.x - b) % c + c) % c + b) : (b = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), c = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), d = this.settings.pullDrag ? -1 * e.x / 5 : 0, f.x = Math.max(Math.min(f.x, b + d), c + d)), this._drag.stage.current = f, this.animate(f.x)) }, e.prototype.onDragEnd = function (b) { var d = this.difference(this._drag.pointer, this.pointer(b)), e = this._drag.stage.current, f = d.x > 0 ^ this.settings.rtl ? "left" : "right"; a(c).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== d.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(e.x, 0 !== d.x ? f : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = f, (Math.abs(d.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () { return !1 })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged")) }, e.prototype.closest = function (b, c) { var e = -1, f = 30, g = this.width(), h = this.coordinates(); return this.settings.freeDrag || a.each(h, a.proxy(function (a, i) { return "left" === c && b > i - f && b < i + f ? e = a : "right" === c && b > i - g - f && b < i - g + f ? e = a + 1 : this.op(b, "<", i) && this.op(b, ">", h[a + 1] !== d ? h[a + 1] : i - g) && (e = "left" === c ? a + 1 : a), -1 === e }, this)), this.settings.loop || (this.op(b, ">", h[this.minimum()]) ? e = b = this.minimum() : this.op(b, "<", h[this.maximum()]) && (e = b = this.maximum())), e }, e.prototype.animate = function (b) { var c = this.speed() > 0; this.is("animating") && this.onTransitionEnd(), c && (this.enter("animating"), this.trigger("translate")), a.support.transform3d && a.support.transition ? this.$stage.css({ transform: "translate3d(" + b + "px,0px,0px)", transition: this.speed() / 1e3 + "s" + (this.settings.slideTransition ? " " + this.settings.slideTransition : "") }) : c ? this.$stage.animate({ left: b + "px" }, this.speed(), this.settings.fallbackEasing, a.proxy(this.onTransitionEnd, this)) : this.$stage.css({ left: b + "px" }) }, e.prototype.is = function (a) { return this._states.current[a] && this._states.current[a] > 0 }, e.prototype.current = function (a) { if (a === d) return this._current; if (0 === this._items.length) return d; if (a = this.normalize(a), this._current !== a) { var b = this.trigger("change", { property: { name: "position", value: a } }); b.data !== d && (a = this.normalize(b.data)), this._current = a, this.invalidate("position"), this.trigger("changed", { property: { name: "position", value: this._current } }) } return this._current }, e.prototype.invalidate = function (b) { return "string" === a.type(b) && (this._invalidated[b] = !0, this.is("valid") && this.leave("valid")), a.map(this._invalidated, function (a, b) { return b }) }, e.prototype.reset = function (a) { (a = this.normalize(a)) !== d && (this._speed = 0, this._current = a, this.suppress(["translate", "translated"]), this.animate(this.coordinates(a)), this.release(["translate", "translated"])) }, e.prototype.normalize = function (a, b) { var c = this._items.length, e = b ? 0 : this._clones.length; return !this.isNumeric(a) || c < 1 ? a = d : (a < 0 || a >= c + e) && (a = ((a - e / 2) % c + c) % c + e / 2), a }, e.prototype.relative = function (a) { return a -= this._clones.length / 2, this.normalize(a, !0) }, e.prototype.maximum = function (a) { var b, c, d, e = this.settings, f = this._coordinates.length; if (e.loop) f = this._clones.length / 2 + this._items.length - 1; else if (e.autoWidth || e.merge) { if (b = this._items.length) for (c = this._items[--b].width(), d = this.$element.width(); b-- && !((c += this._items[b].width() + this.settings.margin) > d);); f = b + 1 } else f = e.center ? this._items.length - 1 : this._items.length - e.items; return a && (f -= this._clones.length / 2), Math.max(f, 0) }, e.prototype.minimum = function (a) { return a ? 0 : this._clones.length / 2 }, e.prototype.items = function (a) { return a === d ? this._items.slice() : (a = this.normalize(a, !0), this._items[a]) }, e.prototype.mergers = function (a) { return a === d ? this._mergers.slice() : (a = this.normalize(a, !0), this._mergers[a]) }, e.prototype.clones = function (b) { var c = this._clones.length / 2, e = c + this._items.length, f = function (a) { return a % 2 == 0 ? e + a / 2 : c - (a + 1) / 2 }; return b === d ? a.map(this._clones, function (a, b) { return f(b) }) : a.map(this._clones, function (a, c) { return a === b ? f(c) : null }) }, e.prototype.speed = function (a) { return a !== d && (this._speed = a), this._speed }, e.prototype.coordinates = function (b) { var c, e = 1, f = b - 1; return b === d ? a.map(this._coordinates, a.proxy(function (a, b) { return this.coordinates(b) }, this)) : (this.settings.center ? (this.settings.rtl && (e = -1, f = b + 1), c = this._coordinates[b], c += (this.width() - c + (this._coordinates[f] || 0)) / 2 * e) : c = this._coordinates[f] || 0, c = Math.ceil(c)) }, e.prototype.duration = function (a, b, c) { return 0 === c ? 0 : Math.min(Math.max(Math.abs(b - a), 1), 6) * Math.abs(c || this.settings.smartSpeed) }, e.prototype.to = function (a, b) { var c = this.current(), d = null, e = a - this.relative(c), f = (e > 0) - (e < 0), g = this._items.length, h = this.minimum(), i = this.maximum(); this.settings.loop ? (!this.settings.rewind && Math.abs(e) > g / 2 && (e += -1 * f * g), a = c + e, (d = ((a - h) % g + g) % g + h) !== a && d - e <= i && d - e > 0 && (c = d - e, a = d, this.reset(c))) : this.settings.rewind ? (i += 1, a = (a % i + i) % i) : a = Math.max(h, Math.min(i, a)), this.speed(this.duration(c, a, b)), this.current(a), this.isVisible() && this.update() }, e.prototype.next = function (a) { a = a || !1, this.to(this.relative(this.current()) + 1, a) }, e.prototype.prev = function (a) { a = a || !1, this.to(this.relative(this.current()) - 1, a) }, e.prototype.onTransitionEnd = function (a) { if (a !== d && (a.stopPropagation(), (a.target || a.srcElement || a.originalTarget) !== this.$stage.get(0))) return !1; this.leave("animating"), this.trigger("translated") }, e.prototype.viewport = function () { var d; return this.options.responsiveBaseElement !== b ? d = a(this.options.responsiveBaseElement).width() : b.innerWidth ? d = b.innerWidth : c.documentElement && c.documentElement.clientWidth ? d = c.documentElement.clientWidth : console.warn("Can not detect viewport width."), d }, e.prototype.replace = function (b) { this.$stage.empty(), this._items = [], b && (b = b instanceof jQuery ? b : a(b)), this.settings.nestedItemSelector && (b = b.find("." + this.settings.nestedItemSelector)), b.filter(function () { return 1 === this.nodeType }).each(a.proxy(function (a, b) { b = this.prepare(b), this.$stage.append(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1) }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items") }, e.prototype.add = function (b, c) { var e = this.relative(this._current); c = c === d ? this._items.length : this.normalize(c, !0), b = b instanceof jQuery ? b : a(b), this.trigger("add", { content: b, position: c }), b = this.prepare(b), 0 === this._items.length || c === this._items.length ? (0 === this._items.length && this.$stage.append(b), 0 !== this._items.length && this._items[c - 1].after(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[c].before(b), this._items.splice(c, 0, b), this._mergers.splice(c, 0, 1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[e] && this.reset(this._items[e].index()), this.invalidate("items"), this.trigger("added", { content: b, position: c }) }, e.prototype.remove = function (a) { (a = this.normalize(a, !0)) !== d && (this.trigger("remove", { content: this._items[a], position: a }), this._items[a].remove(), this._items.splice(a, 1), this._mergers.splice(a, 1), this.invalidate("items"), this.trigger("removed", { content: null, position: a })) }, e.prototype.preloadAutoWidthImages = function (b) { b.each(a.proxy(function (b, c) { this.enter("pre-loading"), c = a(c), a(new Image).one("load", a.proxy(function (a) { c.attr("src", a.target.src), c.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh() }, this)).attr("src", c.attr("src") || c.attr("data-src") || c.attr("data-src-retina")) }, this)) }, e.prototype.destroy = function () { this.$element.off(".owl.core"), this.$stage.off(".owl.core"), a(c).off(".owl.core"), !1 !== this.settings.responsive && (b.clearTimeout(this.resizeTimer), this.off(b, "resize", this._handlers.onThrottledResize)); for (var d in this._plugins) this._plugins[d].destroy(); this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.remove(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel") }, e.prototype.op = function (a, b, c) { var d = this.settings.rtl; switch (b) { case "<": return d ? a > c : a < c; case ">": return d ? a < c : a > c; case ">=": return d ? a <= c : a >= c; case "<=": return d ? a >= c : a <= c } }, e.prototype.on = function (a, b, c, d) { a.addEventListener ? a.addEventListener(b, c, d) : a.attachEvent && a.attachEvent("on" + b, c) }, e.prototype.off = function (a, b, c, d) { a.removeEventListener ? a.removeEventListener(b, c, d) : a.detachEvent && a.detachEvent("on" + b, c) }, e.prototype.trigger = function (b, c, d, f, g) { var h = { item: { count: this._items.length, index: this.current() } }, i = a.camelCase(a.grep(["on", b, d], function (a) { return a }).join("-").toLowerCase()), j = a.Event([b, "owl", d || "carousel"].join(".").toLowerCase(), a.extend({ relatedTarget: this }, h, c)); return this._supress[b] || (a.each(this._plugins, function (a, b) { b.onTrigger && b.onTrigger(j) }), this.register({ type: e.Type.Event, name: b }), this.$element.trigger(j), this.settings && "function" == typeof this.settings[i] && this.settings[i].call(this, j)), j }, e.prototype.enter = function (b) { a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) { this._states.current[b] === d && (this._states.current[b] = 0), this._states.current[b]++ }, this)) }, e.prototype.leave = function (b) { a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) { this._states.current[b]-- }, this)) }, e.prototype.register = function (b) { if (b.type === e.Type.Event) { if (a.event.special[b.name] || (a.event.special[b.name] = {}), !a.event.special[b.name].owl) { var c = a.event.special[b.name]._default; a.event.special[b.name]._default = function (a) { return !c || !c.apply || a.namespace && -1 !== a.namespace.indexOf("owl") ? a.namespace && a.namespace.indexOf("owl") > -1 : c.apply(this, arguments) }, a.event.special[b.name].owl = !0 } } else b.type === e.Type.State && (this._states.tags[b.name] ? this._states.tags[b.name] = this._states.tags[b.name].concat(b.tags) : this._states.tags[b.name] = b.tags, this._states.tags[b.name] = a.grep(this._states.tags[b.name], a.proxy(function (c, d) { return a.inArray(c, this._states.tags[b.name]) === d }, this))) }, e.prototype.suppress = function (b) { a.each(b, a.proxy(function (a, b) { this._supress[b] = !0 }, this)) }, e.prototype.release = function (b) { a.each(b, a.proxy(function (a, b) { delete this._supress[b] }, this)) }, e.prototype.pointer = function (a) { var c = { x: null, y: null }; return a = a.originalEvent || a || b.event, a = a.touches && a.touches.length ? a.touches[0] : a.changedTouches && a.changedTouches.length ? a.changedTouches[0] : a, a.pageX ? (c.x = a.pageX, c.y = a.pageY) : (c.x = a.clientX, c.y = a.clientY), c }, e.prototype.isNumeric = function (a) { return !isNaN(parseFloat(a)) }, e.prototype.difference = function (a, b) { return { x: a.x - b.x, y: a.y - b.y } }, a.fn.owlCarousel = function (b) { var c = Array.prototype.slice.call(arguments, 1); return this.each(function () { var d = a(this), f = d.data("owl.carousel"); f || (f = new e(this, "object" == typeof b && b), d.data("owl.carousel", f), a.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (b, c) { f.register({ type: e.Type.Event, name: c }), f.$element.on(c + ".owl.carousel.core", a.proxy(function (a) { a.namespace && a.relatedTarget !== this && (this.suppress([c]), f[c].apply(this, [].slice.call(arguments, 1)), this.release([c])) }, f)) })), "string" == typeof b && "_" !== b.charAt(0) && f[b].apply(f, c) }) }, a.fn.owlCarousel.Constructor = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { var e = function (b) { this._core = b, this._interval = null, this._visible = null, this._handlers = { "initialized.owl.carousel": a.proxy(function (a) { a.namespace && this._core.settings.autoRefresh && this.watch() }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers) }; e.Defaults = { autoRefresh: !0, autoRefreshInterval: 500 }, e.prototype.watch = function () { this._interval || (this._visible = this._core.isVisible(), this._interval = b.setInterval(a.proxy(this.refresh, this), this._core.settings.autoRefreshInterval)) }, e.prototype.refresh = function () { this._core.isVisible() !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh()) }, e.prototype.destroy = function () { var a, c; b.clearInterval(this._interval); for (a in this._handlers) this._core.$element.off(a, this._handlers[a]); for (c in Object.getOwnPropertyNames(this)) "function" != typeof this[c] && (this[c] = null) }, a.fn.owlCarousel.Constructor.Plugins.AutoRefresh = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { var e = function (b) { this._core = b, this._loaded = [], this._handlers = { "initialized.owl.carousel change.owl.carousel resized.owl.carousel": a.proxy(function (b) { if (b.namespace && this._core.settings && this._core.settings.lazyLoad && (b.property && "position" == b.property.name || "initialized" == b.type)) { var c = this._core.settings, e = c.center && Math.ceil(c.items / 2) || c.items, f = c.center && -1 * e || 0, g = (b.property && b.property.value !== d ? b.property.value : this._core.current()) + f, h = this._core.clones().length, i = a.proxy(function (a, b) { this.load(b) }, this); for (c.lazyLoadEager > 0 && (e += c.lazyLoadEager, c.loop && (g -= c.lazyLoadEager, e++)); f++ < e;)this.load(h / 2 + this._core.relative(g)), h && a.each(this._core.clones(this._core.relative(g)), i), g++ } }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers) }; e.Defaults = { lazyLoad: !1, lazyLoadEager: 0 }, e.prototype.load = function (c) { var d = this._core.$stage.children().eq(c), e = d && d.find(".owl-lazy"); !e || a.inArray(d.get(0), this._loaded) > -1 || (e.each(a.proxy(function (c, d) { var e, f = a(d), g = b.devicePixelRatio > 1 && f.attr("data-src-retina") || f.attr("data-src") || f.attr("data-srcset"); this._core.trigger("load", { element: f, url: g }, "lazy"), f.is("img") ? f.one("load.owl.lazy", a.proxy(function () { f.css("opacity", 1), this._core.trigger("loaded", { element: f, url: g }, "lazy") }, this)).attr("src", g) : f.is("source") ? f.one("load.owl.lazy", a.proxy(function () { this._core.trigger("loaded", { element: f, url: g }, "lazy") }, this)).attr("srcset", g) : (e = new Image, e.onload = a.proxy(function () { f.css({ "background-image": 'url("' + g + '")', opacity: "1" }), this._core.trigger("loaded", { element: f, url: g }, "lazy") }, this), e.src = g) }, this)), this._loaded.push(d.get(0))) }, e.prototype.destroy = function () { var a, b; for (a in this.handlers) this._core.$element.off(a, this.handlers[a]); for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null) }, a.fn.owlCarousel.Constructor.Plugins.Lazy = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { var e = function (c) { this._core = c, this._previousHeight = null, this._handlers = { "initialized.owl.carousel refreshed.owl.carousel": a.proxy(function (a) { a.namespace && this._core.settings.autoHeight && this.update() }, this), "changed.owl.carousel": a.proxy(function (a) { a.namespace && this._core.settings.autoHeight && "position" === a.property.name && this.update() }, this), "loaded.owl.lazy": a.proxy(function (a) { a.namespace && this._core.settings.autoHeight && a.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update() }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers), this._intervalId = null; var d = this; a(b).on("load", function () { d._core.settings.autoHeight && d.update() }), a(b).resize(function () { d._core.settings.autoHeight && (null != d._intervalId && clearTimeout(d._intervalId), d._intervalId = setTimeout(function () { d.update() }, 250)) }) }; e.Defaults = { autoHeight: !1, autoHeightClass: "owl-height" }, e.prototype.update = function () { var b = this._core._current, c = b + this._core.settings.items, d = this._core.settings.lazyLoad, e = this._core.$stage.children().toArray().slice(b, c), f = [], g = 0; a.each(e, function (b, c) { f.push(a(c).height()) }), g = Math.max.apply(null, f), g <= 1 && d && this._previousHeight && (g = this._previousHeight), this._previousHeight = g, this._core.$stage.parent().height(g).addClass(this._core.settings.autoHeightClass) }, e.prototype.destroy = function () { var a, b; for (a in this._handlers) this._core.$element.off(a, this._handlers[a]); for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null) }, a.fn.owlCarousel.Constructor.Plugins.AutoHeight = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { var e = function (b) { this._core = b, this._videos = {}, this._playing = null, this._handlers = { "initialized.owl.carousel": a.proxy(function (a) { a.namespace && this._core.register({ type: "state", name: "playing", tags: ["interacting"] }) }, this), "resize.owl.carousel": a.proxy(function (a) { a.namespace && this._core.settings.video && this.isInFullScreen() && a.preventDefault() }, this), "refreshed.owl.carousel": a.proxy(function (a) { a.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove() }, this), "changed.owl.carousel": a.proxy(function (a) { a.namespace && "position" === a.property.name && this._playing && this.stop() }, this), "prepared.owl.carousel": a.proxy(function (b) { if (b.namespace) { var c = a(b.content).find(".owl-video"); c.length && (c.css("display", "none"), this.fetch(c, a(b.content))) } }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", a.proxy(function (a) { this.play(a) }, this)) }; e.Defaults = { video: !1, videoHeight: !1, videoWidth: !1 }, e.prototype.fetch = function (a, b) { var c = function () { return a.attr("data-vimeo-id") ? "vimeo" : a.attr("data-vzaar-id") ? "vzaar" : "youtube" }(), d = a.attr("data-vimeo-id") || a.attr("data-youtube-id") || a.attr("data-vzaar-id"), e = a.attr("data-width") || this._core.settings.videoWidth, f = a.attr("data-height") || this._core.settings.videoHeight, g = a.attr("href"); if (!g) throw new Error("Missing video URL."); if (d = g.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), d[3].indexOf("youtu") > -1) c = "youtube"; else if (d[3].indexOf("vimeo") > -1) c = "vimeo"; else { if (!(d[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported."); c = "vzaar" } d = d[6], this._videos[g] = { type: c, id: d, width: e, height: f }, b.attr("data-video", g), this.thumbnail(a, this._videos[g]) }, e.prototype.thumbnail = function (b, c) { var d, e, f, g = c.width && c.height ? "width:" + c.width + "px;height:" + c.height + "px;" : "", h = b.find("img"), i = "src", j = "", k = this._core.settings, l = function (c) { e = '<div class="owl-video-play-icon"></div>', d = k.lazyLoad ? a("<div/>", { class: "owl-video-tn " + j, srcType: c }) : a("<div/>", { class: "owl-video-tn", style: "opacity:1;background-image:url(" + c + ")" }), b.after(d), b.after(e) }; if (b.wrap(a("<div/>", { class: "owl-video-wrapper", style: g })), this._core.settings.lazyLoad && (i = "data-src", j = "owl-lazy"), h.length) return l(h.attr(i)), h.remove(), !1; "youtube" === c.type ? (f = "//img.youtube.com/vi/" + c.id + "/hqdefault.jpg", l(f)) : "vimeo" === c.type ? a.ajax({ type: "GET", url: "//vimeo.com/api/v2/video/" + c.id + ".json", jsonp: "callback", dataType: "jsonp", success: function (a) { f = a[0].thumbnail_large, l(f) } }) : "vzaar" === c.type && a.ajax({ type: "GET", url: "//vzaar.com/api/videos/" + c.id + ".json", jsonp: "callback", dataType: "jsonp", success: function (a) { f = a.framegrab_url, l(f) } }) }, e.prototype.stop = function () { this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video") }, e.prototype.play = function (b) { var c, d = a(b.target), e = d.closest("." + this._core.settings.itemClass), f = this._videos[e.attr("data-video")], g = f.width || "100%", h = f.height || this._core.$stage.height(); this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), e = this._core.items(this._core.relative(e.index())), this._core.reset(e.index()), c = a('<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>'), c.attr("height", h), c.attr("width", g), "youtube" === f.type ? c.attr("src", "//www.youtube.com/embed/" + f.id + "?autoplay=1&rel=0&v=" + f.id) : "vimeo" === f.type ? c.attr("src", "//player.vimeo.com/video/" + f.id + "?autoplay=1") : "vzaar" === f.type && c.attr("src", "//view.vzaar.com/" + f.id + "/player?autoplay=true"), a(c).wrap('<div class="owl-video-frame" />').insertAfter(e.find(".owl-video")), this._playing = e.addClass("owl-video-playing")) }, e.prototype.isInFullScreen = function () { var b = c.fullscreenElement || c.mozFullScreenElement || c.webkitFullscreenElement; return b && a(b).parent().hasClass("owl-video-frame") }, e.prototype.destroy = function () { var a, b; this._core.$element.off("click.owl.video"); for (a in this._handlers) this._core.$element.off(a, this._handlers[a]); for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null) }, a.fn.owlCarousel.Constructor.Plugins.Video = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    var e = function (b) { this.core = b, this.core.options = a.extend({}, e.Defaults, this.core.options), this.swapping = !0, this.previous = d, this.next = d, this.handlers = { "change.owl.carousel": a.proxy(function (a) { a.namespace && "position" == a.property.name && (this.previous = this.core.current(), this.next = a.property.value) }, this), "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": a.proxy(function (a) { a.namespace && (this.swapping = "translated" == a.type) }, this), "translate.owl.carousel": a.proxy(function (a) { a.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap() }, this) }, this.core.$element.on(this.handlers) }; e.Defaults = {
        animateOut: !1,
        animateIn: !1
    }, e.prototype.swap = function () { if (1 === this.core.settings.items && a.support.animation && a.support.transition) { this.core.speed(0); var b, c = a.proxy(this.clear, this), d = this.core.$stage.children().eq(this.previous), e = this.core.$stage.children().eq(this.next), f = this.core.settings.animateIn, g = this.core.settings.animateOut; this.core.current() !== this.previous && (g && (b = this.core.coordinates(this.previous) - this.core.coordinates(this.next), d.one(a.support.animation.end, c).css({ left: b + "px" }).addClass("animated owl-animated-out").addClass(g)), f && e.one(a.support.animation.end, c).addClass("animated owl-animated-in").addClass(f)) } }, e.prototype.clear = function (b) { a(b.target).css({ left: "" }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd() }, e.prototype.destroy = function () { var a, b; for (a in this.handlers) this.core.$element.off(a, this.handlers[a]); for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null) }, a.fn.owlCarousel.Constructor.Plugins.Animate = e
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { var e = function (b) { this._core = b, this._call = null, this._time = 0, this._timeout = 0, this._paused = !0, this._handlers = { "changed.owl.carousel": a.proxy(function (a) { a.namespace && "settings" === a.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : a.namespace && "position" === a.property.name && this._paused && (this._time = 0) }, this), "initialized.owl.carousel": a.proxy(function (a) { a.namespace && this._core.settings.autoplay && this.play() }, this), "play.owl.autoplay": a.proxy(function (a, b, c) { a.namespace && this.play(b, c) }, this), "stop.owl.autoplay": a.proxy(function (a) { a.namespace && this.stop() }, this), "mouseover.owl.autoplay": a.proxy(function () { this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause() }, this), "mouseleave.owl.autoplay": a.proxy(function () { this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play() }, this), "touchstart.owl.core": a.proxy(function () { this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause() }, this), "touchend.owl.core": a.proxy(function () { this._core.settings.autoplayHoverPause && this.play() }, this) }, this._core.$element.on(this._handlers), this._core.options = a.extend({}, e.Defaults, this._core.options) }; e.Defaults = { autoplay: !1, autoplayTimeout: 5e3, autoplayHoverPause: !1, autoplaySpeed: !1 }, e.prototype._next = function (d) { this._call = b.setTimeout(a.proxy(this._next, this, d), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()), this._core.is("interacting") || c.hidden || this._core.next(d || this._core.settings.autoplaySpeed) }, e.prototype.read = function () { return (new Date).getTime() - this._time }, e.prototype.play = function (c, d) { var e; this._core.is("rotating") || this._core.enter("rotating"), c = c || this._core.settings.autoplayTimeout, e = Math.min(this._time % (this._timeout || c), c), this._paused ? (this._time = this.read(), this._paused = !1) : b.clearTimeout(this._call), this._time += this.read() % c - e, this._timeout = c, this._call = b.setTimeout(a.proxy(this._next, this, d), c - e) }, e.prototype.stop = function () { this._core.is("rotating") && (this._time = 0, this._paused = !0, b.clearTimeout(this._call), this._core.leave("rotating")) }, e.prototype.pause = function () { this._core.is("rotating") && !this._paused && (this._time = this.read(), this._paused = !0, b.clearTimeout(this._call)) }, e.prototype.destroy = function () { var a, b; this.stop(); for (a in this._handlers) this._core.$element.off(a, this._handlers[a]); for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null) }, a.fn.owlCarousel.Constructor.Plugins.autoplay = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { "use strict"; var e = function (b) { this._core = b, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = { next: this._core.next, prev: this._core.prev, to: this._core.to }, this._handlers = { "prepared.owl.carousel": a.proxy(function (b) { b.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + a(b.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>") }, this), "added.owl.carousel": a.proxy(function (a) { a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 0, this._templates.pop()) }, this), "remove.owl.carousel": a.proxy(function (a) { a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 1) }, this), "changed.owl.carousel": a.proxy(function (a) { a.namespace && "position" == a.property.name && this.draw() }, this), "initialized.owl.carousel": a.proxy(function (a) { a.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation")) }, this), "refreshed.owl.carousel": a.proxy(function (a) { a.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation")) }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers) }; e.Defaults = { nav: !1, navText: ['<span aria-label="Previous">&#x2039;</span>', '<span aria-label="Next">&#x203a;</span>'], navSpeed: !1, navElement: 'button type="button" role="presentation"', navContainer: !1, navContainerClass: "owl-nav", navClass: ["owl-prev", "owl-next"], slideBy: 1, dotClass: "owl-dot", dotsClass: "owl-dots", dots: !0, dotsEach: !1, dotsData: !1, dotsSpeed: !1, dotsContainer: !1 }, e.prototype.initialize = function () { var b, c = this._core.settings; this._controls.$relative = (c.navContainer ? a(c.navContainer) : a("<div>").addClass(c.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = a("<" + c.navElement + ">").addClass(c.navClass[0]).html(c.navText[0]).prependTo(this._controls.$relative).on("click", a.proxy(function (a) { this.prev(c.navSpeed) }, this)), this._controls.$next = a("<" + c.navElement + ">").addClass(c.navClass[1]).html(c.navText[1]).appendTo(this._controls.$relative).on("click", a.proxy(function (a) { this.next(c.navSpeed) }, this)), c.dotsData || (this._templates = [a('<button role="button">').addClass(c.dotClass).append(a("<span>")).prop("outerHTML")]), this._controls.$absolute = (c.dotsContainer ? a(c.dotsContainer) : a("<div>").addClass(c.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "button", a.proxy(function (b) { var d = a(b.target).parent().is(this._controls.$absolute) ? a(b.target).index() : a(b.target).parent().index(); b.preventDefault(), this.to(d, c.dotsSpeed) }, this)); for (b in this._overrides) this._core[b] = a.proxy(this[b], this) }, e.prototype.destroy = function () { var a, b, c, d, e; e = this._core.settings; for (a in this._handlers) this.$element.off(a, this._handlers[a]); for (b in this._controls) "$relative" === b && e.navContainer ? this._controls[b].html("") : this._controls[b].remove(); for (d in this.overides) this._core[d] = this._overrides[d]; for (c in Object.getOwnPropertyNames(this)) "function" != typeof this[c] && (this[c] = null) }, e.prototype.update = function () { var a, b, c, d = this._core.clones().length / 2, e = d + this._core.items().length, f = this._core.maximum(!0), g = this._core.settings, h = g.center || g.autoWidth || g.dotsData ? 1 : g.dotsEach || g.items; if ("page" !== g.slideBy && (g.slideBy = Math.min(g.slideBy, g.items)), g.dots || "page" == g.slideBy) for (this._pages = [], a = d, b = 0, c = 0; a < e; a++) { if (b >= h || 0 === b) { if (this._pages.push({ start: Math.min(f, a - d), end: a - d + h - 1 }), Math.min(f, a - d) === f) break; b = 0, ++c } b += this._core.mergers(this._core.relative(a)) } }, e.prototype.draw = function () { var b, c = this._core.settings, d = this._core.items().length <= c.items, e = this._core.relative(this._core.current()), f = c.loop || c.rewind; this._controls.$relative.toggleClass("disabled", !c.nav || d), c.nav && (this._controls.$previous.toggleClass("disabled", !f && e <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !f && e >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !c.dots || d), c.dots && (b = this._pages.length - this._controls.$absolute.children().length, c.dotsData && 0 !== b ? this._controls.$absolute.html(this._templates.join("")) : b > 0 ? this._controls.$absolute.append(new Array(b + 1).join(this._templates[0])) : b < 0 && this._controls.$absolute.children().slice(b).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(a.inArray(this.current(), this._pages)).addClass("active")) }, e.prototype.onTrigger = function (b) { var c = this._core.settings; b.page = { index: a.inArray(this.current(), this._pages), count: this._pages.length, size: c && (c.center || c.autoWidth || c.dotsData ? 1 : c.dotsEach || c.items) } }, e.prototype.current = function () { var b = this._core.relative(this._core.current()); return a.grep(this._pages, a.proxy(function (a, c) { return a.start <= b && a.end >= b }, this)).pop() }, e.prototype.getPosition = function (b) { var c, d, e = this._core.settings; return "page" == e.slideBy ? (c = a.inArray(this.current(), this._pages), d = this._pages.length, b ? ++c : --c, c = this._pages[(c % d + d) % d].start) : (c = this._core.relative(this._core.current()), d = this._core.items().length, b ? c += e.slideBy : c -= e.slideBy), c }, e.prototype.next = function (b) { a.proxy(this._overrides.to, this._core)(this.getPosition(!0), b) }, e.prototype.prev = function (b) { a.proxy(this._overrides.to, this._core)(this.getPosition(!1), b) }, e.prototype.to = function (b, c, d) { var e; !d && this._pages.length ? (e = this._pages.length, a.proxy(this._overrides.to, this._core)(this._pages[(b % e + e) % e].start, c)) : a.proxy(this._overrides.to, this._core)(b, c) }, a.fn.owlCarousel.Constructor.Plugins.Navigation = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { "use strict"; var e = function (c) { this._core = c, this._hashes = {}, this.$element = this._core.$element, this._handlers = { "initialized.owl.carousel": a.proxy(function (c) { c.namespace && "URLHash" === this._core.settings.startPosition && a(b).trigger("hashchange.owl.navigation") }, this), "prepared.owl.carousel": a.proxy(function (b) { if (b.namespace) { var c = a(b.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash"); if (!c) return; this._hashes[c] = b.content } }, this), "changed.owl.carousel": a.proxy(function (c) { if (c.namespace && "position" === c.property.name) { var d = this._core.items(this._core.relative(this._core.current())), e = a.map(this._hashes, function (a, b) { return a === d ? b : null }).join(); if (!e || b.location.hash.slice(1) === e) return; b.location.hash = e } }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers), a(b).on("hashchange.owl.navigation", a.proxy(function (a) { var c = b.location.hash.substring(1), e = this._core.$stage.children(), f = this._hashes[c] && e.index(this._hashes[c]); f !== d && f !== this._core.current() && this._core.to(this._core.relative(f), !1, !0) }, this)) }; e.Defaults = { URLhashListener: !1 }, e.prototype.destroy = function () { var c, d; a(b).off("hashchange.owl.navigation"); for (c in this._handlers) this._core.$element.off(c, this._handlers[c]); for (d in Object.getOwnPropertyNames(this)) "function" != typeof this[d] && (this[d] = null) }, a.fn.owlCarousel.Constructor.Plugins.Hash = e }(window.Zepto || window.jQuery, window, document), function (a, b, c, d) { function e(b, c) { var e = !1, f = b.charAt(0).toUpperCase() + b.slice(1); return a.each((b + " " + h.join(f + " ") + f).split(" "), function (a, b) { if (g[b] !== d) return e = !c || b, !1 }), e } function f(a) { return e(a, !0) } var g = a("<support>").get(0).style, h = "Webkit Moz O ms".split(" "), i = { transition: { end: { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd", transition: "transitionend" } }, animation: { end: { WebkitAnimation: "webkitAnimationEnd", MozAnimation: "animationend", OAnimation: "oAnimationEnd", animation: "animationend" } } }, j = { csstransforms: function () { return !!e("transform") }, csstransforms3d: function () { return !!e("perspective") }, csstransitions: function () { return !!e("transition") }, cssanimations: function () { return !!e("animation") } }; j.csstransitions() && (a.support.transition = new String(f("transition")), a.support.transition.end = i.transition.end[a.support.transition]), j.cssanimations() && (a.support.animation = new String(f("animation")), a.support.animation.end = i.animation.end[a.support.animation]), j.csstransforms() && (a.support.transform = new String(f("transform")), a.support.transform3d = j.csstransforms3d()) }(window.Zepto || window.jQuery, window, document);


/*!
 * classie v1.0.1
 * class helper functions
 * from bonzo https://github.com/ded/bonzo
 * MIT license
 *
 * classie.has( elem, 'my-class' ) -> true/false
 * classie.add( elem, 'my-new-class' )
 * classie.remove( elem, 'my-unwanted-class' )
 * classie.toggle( elem, 'my-class' )
 */

/*jshint browser: true, strict: true, undef: true, unused: true */
/*global define: false, module: false */

(function (window) {

    'use strict';

    // class helper functions from bonzo https://github.com/ded/bonzo

    function classReg(className) {
        return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
    }

    // classList support for class management
    // altho to be fair, the api sucks because it won't accept multiple classes at once
    var hasClass, addClass, removeClass;

    if ('classList' in document.documentElement) {
        hasClass = function (elem, c) {
            return elem.classList.contains(c);
        };
        addClass = function (elem, c) {
            elem.classList.add(c);
        };
        removeClass = function (elem, c) {
            elem.classList.remove(c);
        };
    } else {
        hasClass = function (elem, c) {
            return classReg(c).test(elem.className);
        };
        addClass = function (elem, c) {
            if (!hasClass(elem, c)) {
                elem.className = elem.className + ' ' + c;
            }
        };
        removeClass = function (elem, c) {
            elem.className = elem.className.replace(classReg(c), ' ');
        };
    }

    function toggleClass(elem, c) {
        var fn = hasClass(elem, c) ? removeClass : addClass;
        fn(elem, c);
    }

    var classie = {
        // full names
        hasClass: hasClass,
        addClass: addClass,
        removeClass: removeClass,
        toggleClass: toggleClass,
        // short names
        has: hasClass,
        add: addClass,
        remove: removeClass,
        toggle: toggleClass
    };

    // transport
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(classie);
    } else if (typeof exports === 'object') {
        // CommonJS
        module.exports = classie;
    } else {
        // browser global
        window.classie = classie;
    }

})(window);

(function () {
    var t, e, n, r, i, o, s, a, l, u, f, h, c, p, m, d, g, y, v, b, w, x, M, k, S, T, C, F, H, R, q, X, Y, j, z, I, A, G, V, Z, E, O, L, D, P, W, N, $, B, U, K, J, Q, _, te, ee, ne = function (t, e) {
        return function () {
            return t.apply(e, arguments)
        }
    };
    H = function () {
        return "visible" === document.visibilityState || null != T.tests
    }, j = function () {
        var t;
        return t = [], "undefined" != typeof document && null !== document && document.addEventListener("visibilitychange", function () {
                var e, n, r, i;
                for (i = [], n = 0, r = t.length; r > n; n++) e = t[n], i.push(e(H()));
                return i
            }),
            function (e) {
                return t.push(e)
            }
    }(), x = function (t) {
        var e, n, r;
        n = {};
        for (e in t) r = t[e], n[e] = r;
        return n
    }, b = function (t) {
        var e;
        return e = {},
            function () {
                var n, r, i, o, s;
                for (r = "", o = 0, s = arguments.length; s > o; o++) n = arguments[o], r += n.toString() + ",";
                return i = e[r], i || (e[r] = i = t.apply(this, arguments)), i
            }
    }, Y = function (t) {
        return function (e) {
            var n, r, i;
            return e instanceof Array || e instanceof NodeList || e instanceof HTMLCollection ? i = function () {
                var i, o, s;
                for (s = [], r = i = 0, o = e.length; o >= 0 ? o > i : i > o; r = o >= 0 ? ++i : --i) n = Array.prototype.slice.call(arguments, 1), n.splice(0, 0, e[r]), s.push(t.apply(this, n));
                return s
            }.apply(this, arguments) : t.apply(this, arguments)
        }
    }, d = function (t, e) {
        var n, r, i;
        i = [];
        for (n in e) r = e[n], i.push(null != t[n] ? t[n] : t[n] = r);
        return i
    }, g = function (t, e) {
        var n, r, i;
        if (null != t.style) return y(t, e);
        i = [];
        for (n in e) r = e[n], i.push(t[n] = r.format());
        return i
    }, y = function (t, e) {
        var n, r, i, o, s;
        e = z(e), o = [], n = R(t);
        for (r in e) s = e[r], _.contains(r) ? o.push([r, s]) : (s = null != s.format ? s.format() : "" + s + ee(r, s), n && B.contains(r) ? t.setAttribute(r, s) : t.style[A(r)] = s);
        return o.length > 0 ? n ? (i = new l, i.applyProperties(o), t.setAttribute("transform", i.decompose().format())) : (s = o.map(function (t) {
            return te(t[0], t[1])
        }).join(" "), t.style[A("transform")] = s) : void 0
    }, R = function (t) {
        var e, n;
        return "undefined" != typeof SVGElement && null !== SVGElement && "undefined" != typeof SVGSVGElement && null !== SVGSVGElement ? t instanceof SVGElement && !(t instanceof SVGSVGElement) : null != (e = null != (n = T.tests) && "function" == typeof n.isSVG ? n.isSVG(t) : void 0) ? e : !1
    }, Z = function (t, e) {
        var n;
        return n = Math.pow(10, e), Math.round(t * n) / n
    }, u = function () {
        function t(t) {
            var e, n, r;
            for (this.obj = {}, n = 0, r = t.length; r > n; n++) e = t[n], this.obj[e] = 1
        }
        return t.prototype.contains = function (t) {
            return 1 === this.obj[t]
        }, t
    }(), Q = function (t) {
        return t.replace(/([A-Z])/g, function (t) {
            return "-" + t.toLowerCase()
        })
    }, G = new u("marginTop,marginLeft,marginBottom,marginRight,paddingTop,paddingLeft,paddingBottom,paddingRight,top,left,bottom,right,translateX,translateY,translateZ,perspectiveX,perspectiveY,perspectiveZ,width,height,maxWidth,maxHeight,minWidth,minHeight,borderRadius".split(",")), S = new u("rotate,rotateX,rotateY,rotateZ,skew,skewX,skewY,skewZ".split(",")), _ = new u("translate,translateX,translateY,translateZ,scale,scaleX,scaleY,scaleZ,rotate,rotateX,rotateY,rotateZ,rotateC,rotateCX,rotateCY,skew,skewX,skewY,skewZ,perspective".split(",")), B = new u("accent-height,ascent,azimuth,baseFrequency,baseline-shift,bias,cx,cy,d,diffuseConstant,divisor,dx,dy,elevation,filterRes,fx,fy,gradientTransform,height,k1,k2,k3,k4,kernelMatrix,kernelUnitLength,letter-spacing,limitingConeAngle,markerHeight,markerWidth,numOctaves,order,overline-position,overline-thickness,pathLength,points,pointsAtX,pointsAtY,pointsAtZ,r,radius,rx,ry,seed,specularConstant,specularExponent,stdDeviation,stop-color,stop-opacity,strikethrough-position,strikethrough-thickness,surfaceScale,target,targetX,targetY,transform,underline-position,underline-thickness,viewBox,width,x,x1,x2,y,y1,y2,z".split(",")), ee = function (t, e) {
        return "number" != typeof e ? "" : G.contains(t) ? "px" : S.contains(t) ? "deg" : ""
    }, te = function (t, e) {
        var n, r;
        return n = ("" + e).match(/^([0-9.-]*)([^0-9]*)$/), null != n ? (e = n[1], r = n[2]) : e = parseFloat(e), e = Z(parseFloat(e), 10), (null == r || "" === r) && (r = ee(t, e)), "" + t + "(" + e + r + ")"
    }, z = function (t) {
        var e, n, r, i, o, s, a, l;
        r = {};
        for (i in t)
            if (o = t[i], _.contains(i))
                if (n = i.match(/(translate|rotateC|rotate|skew|scale|perspective)(X|Y|Z|)/), n && n[2].length > 0) r[i] = o;
                else
                    for (l = ["X", "Y", "Z"], s = 0, a = l.length; a > s; s++) e = l[s], r[n[1] + e] = o;
        else r[i] = o;
        return r
    }, k = function (t) {
        var e;
        return e = "opacity" === t ? 1 : 0, "" + e + ee(t, e)
    }, C = function (t, e) {
        var n, r, i, o, s, u, f, h, c, p, m;
        if (o = {}, n = R(t), null != t.style)
            for (s = window.getComputedStyle(t, null), f = 0, c = e.length; c > f; f++) r = e[f], _.contains(r) ? null == o.transform && (i = n ? new l(null != (m = t.transform.baseVal.consolidate()) ? m.matrix : void 0) : a.fromTransform(s[A("transform")]), o.transform = i.decompose()) : (u = s[r], null == u && B.contains(r) && (u = t.getAttribute(r)), ("" === u || null == u) && (u = k(r)), o[r] = M(u));
        else
            for (h = 0, p = e.length; p > h; h++) r = e[h], o[r] = M(t[r]);
        return o
    }, M = function (t) {
        var e, n, a, l, u;
        for (a = [i, r, o, s], l = 0, u = a.length; u > l; l++)
            if (n = a[l], e = n.create(t), null != e) return e;
        return null
    }, o = function () {
        function t(t) {
            this.format = ne(this.format, this), this.interpolate = ne(this.interpolate, this), this.obj = t
        }
        return t.prototype.interpolate = function (e, n) {
            var r, i, o, s, a;
            s = this.obj, r = e.obj, o = {};
            for (i in s) a = s[i], o[i] = null != a.interpolate ? a.interpolate(r[i], n) : a;
            return new t(o)
        }, t.prototype.format = function () {
            return this.obj
        }, t.create = function (e) {
            var n, r, i;
            if (e instanceof Object) {
                r = {};
                for (n in e) i = e[n], r[n] = M(i);
                return new t(r)
            }
            return null
        }, t
    }(), s = function () {
        function t(t, e, n) {
            this.prefix = e, this.suffix = n, this.format = ne(this.format, this), this.interpolate = ne(this.interpolate, this), this.value = parseFloat(t)
        }
        return t.prototype.interpolate = function (e, n) {
            var r, i;
            return i = this.value, r = e.value, new t((r - i) * n + i, e.prefix || this.prefix, e.suffix || this.suffix)
        }, t.prototype.format = function () {
            return null == this.prefix && null == this.suffix ? Z(this.value, 5) : this.prefix + Z(this.value, 5) + this.suffix
        }, t.create = function (e) {
            var n;
            return "string" != typeof e ? new t(e) : (n = ("" + e).match("([^0-9.+-]*)([0-9.+-]+)([^0-9.+-]*)"), null != n ? new t(n[2], n[1], n[3]) : null)
        }, t
    }(), r = function () {
        function t(t, e) {
            this.values = t, this.sep = e, this.format = ne(this.format, this), this.interpolate = ne(this.interpolate, this)
        }
        return t.prototype.interpolate = function (e, n) {
            var r, i, o, s, a, l;
            for (s = this.values, r = e.values, o = [], i = a = 0, l = Math.min(s.length, r.length); l >= 0 ? l > a : a > l; i = l >= 0 ? ++a : --a) o.push(null != s[i].interpolate ? s[i].interpolate(r[i], n) : s[i]);
            return new t(o, this.sep)
        }, t.prototype.format = function () {
            var t;
            return t = this.values.map(function (t) {
                return null != t.format ? t.format() : t
            }), null != this.sep ? t.join(this.sep) : t
        }, t.createFromArray = function (e, n) {
            var r;
            return r = e.map(function (t) {
                return M(t) || t
            }), r = r.filter(function (t) {
                return null != t
            }), new t(r, n)
        }, t.create = function (e) {
            var n, r, i, o, s;
            if (e instanceof Array) return t.createFromArray(e, null);
            if ("string" == typeof e) {
                for (i = [" ", ",", "|", ";", "/", ":"], o = 0, s = i.length; s > o; o++)
                    if (r = i[o], n = e.split(r), n.length > 1) return t.createFromArray(n, r);
                return null
            }
        }, t
    }(), t = function () {
        function t(t, e) {
            this.rgb = null != t ? t : {}, this.format = e, this.toRgba = ne(this.toRgba, this), this.toRgb = ne(this.toRgb, this), this.toHex = ne(this.toHex, this)
        }
        return t.fromHex = function (e) {
            var n, r;
            return n = e.match(/^#([a-f\d]{1})([a-f\d]{1})([a-f\d]{1})$/i), null != n && (e = "#" + n[1] + n[1] + n[2] + n[2] + n[3] + n[3]), r = e.match(/^#([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i), null != r ? new t({
                r: parseInt(r[1], 16),
                g: parseInt(r[2], 16),
                b: parseInt(r[3], 16),
                a: 1
            }, "hex") : null
        }, t.fromRgb = function (e) {
            var n, r;
            return n = e.match(/^rgba?\(([0-9.]*), ?([0-9.]*), ?([0-9.]*)(?:, ?([0-9.]*))?\)$/), null != n ? new t({
                r: parseFloat(n[1]),
                g: parseFloat(n[2]),
                b: parseFloat(n[3]),
                a: parseFloat(null != (r = n[4]) ? r : 1)
            }, null != n[4] ? "rgba" : "rgb") : null
        }, t.componentToHex = function (t) {
            var e;
            return e = t.toString(16), 1 === e.length ? "0" + e : e
        }, t.prototype.toHex = function () {
            return "#" + t.componentToHex(this.rgb.r) + t.componentToHex(this.rgb.g) + t.componentToHex(this.rgb.b)
        }, t.prototype.toRgb = function () {
            return "rgb(" + this.rgb.r + ", " + this.rgb.g + ", " + this.rgb.b + ")"
        }, t.prototype.toRgba = function () {
            return "rgba(" + this.rgb.r + ", " + this.rgb.g + ", " + this.rgb.b + ", " + this.rgb.a + ")"
        }, t
    }(), i = function () {
        function e(t) {
            this.color = t, this.format = ne(this.format, this), this.interpolate = ne(this.interpolate, this)
        }
        return e.prototype.interpolate = function (n, r) {
            var i, o, s, a, l, u, f, h;
            for (a = this.color, i = n.color, s = {}, h = ["r", "g", "b"], u = 0, f = h.length; f > u; u++) o = h[u], l = Math.round((i.rgb[o] - a.rgb[o]) * r + a.rgb[o]), s[o] = Math.min(255, Math.max(0, l));
            return o = "a", l = Z((i.rgb[o] - a.rgb[o]) * r + a.rgb[o], 5), s[o] = Math.min(1, Math.max(0, l)), new e(new t(s, i.format))
        }, e.prototype.format = function () {
            return "hex" === this.color.format ? this.color.toHex() : "rgb" === this.color.format ? this.color.toRgb() : "rgba" === this.color.format ? this.color.toRgba() : void 0
        }, e.create = function (n) {
            var r;
            if ("string" == typeof n) return r = t.fromHex(n) || t.fromRgb(n), null != r ? new e(r) : null
        }, e
    }(), n = function () {
        function t(t) {
            this.props = t, this.applyRotateCenter = ne(this.applyRotateCenter, this), this.format = ne(this.format, this), this.interpolate = ne(this.interpolate, this)
        }
        return t.prototype.interpolate = function (e, n) {
            var r, i, o, s, a, l, u, f, h, c, p, m;
            for (o = {}, c = ["translate", "scale", "rotate"], s = 0, f = c.length; f > s; s++)
                for (i = c[s], o[i] = [], r = a = 0, p = this.props[i].length; p >= 0 ? p > a : a > p; r = p >= 0 ? ++a : --a) o[i][r] = (e.props[i][r] - this.props[i][r]) * n + this.props[i][r];
            for (r = l = 1; 2 >= l; r = ++l) o.rotate[r] = e.props.rotate[r];
            for (m = ["skew"], u = 0, h = m.length; h > u; u++) i = m[u], o[i] = (e.props[i] - this.props[i]) * n + this.props[i];
            return new t(o)
        }, t.prototype.format = function () {
            return "translate(" + this.props.translate.join(",") + ") rotate(" + this.props.rotate.join(",") + ") skewX(" + this.props.skew + ") scale(" + this.props.scale.join(",") + ")"
        }, t.prototype.applyRotateCenter = function (t) {
            var e, n, r, i, o, s;
            for (n = v.createSVGMatrix(), n = n.translate(t[0], t[1]), n = n.rotate(this.props.rotate[0]), n = n.translate(-t[0], -t[1]), r = new l(n), i = r.decompose().props.translate, s = [], e = o = 0; 1 >= o; e = ++o) s.push(this.props.translate[e] -= i[e]);
            return s
        }, t
    }(), v = "undefined" != typeof document && null !== document ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : void 0, l = function () {
        function t(t) {
            this.m = t, this.applyProperties = ne(this.applyProperties, this), this.decompose = ne(this.decompose, this), this.m || (this.m = v.createSVGMatrix())
        }
        return t.prototype.decompose = function () {
            var t, e, r, i, o;
            return i = new f([this.m.a, this.m.b]), o = new f([this.m.c, this.m.d]), t = i.length(), r = i.dot(o), i = i.normalize(), e = o.combine(i, 1, -r).length(), new n({
                translate: [this.m.e, this.m.f],
                rotate: [180 * Math.atan2(this.m.b, this.m.a) / Math.PI, this.rotateCX, this.rotateCY],
                scale: [t, e],
                skew: r / e * 180 / Math.PI
            })
        }, t.prototype.applyProperties = function (t) {
            var e, n, r, i, o, s, a, l;
            for (e = {}, o = 0, s = t.length; s > o; o++) r = t[o], e[r[0]] = r[1];
            for (n in e) i = e[n], "translateX" === n ? this.m = this.m.translate(i, 0) : "translateY" === n ? this.m = this.m.translate(0, i) : "scaleX" === n ? this.m = this.m.scale(i, 1) : "scaleY" === n ? this.m = this.m.scale(1, i) : "rotateZ" === n ? this.m = this.m.rotate(i) : "skewX" === n ? this.m = this.m.skewX(i) : "skewY" === n && (this.m = this.m.skewY(i));
            return this.rotateCX = null != (a = e.rotateCX) ? a : 0, this.rotateCY = null != (l = e.rotateCY) ? l : 0
        }, t
    }(), f = function () {
        function t(t) {
            this.els = t, this.combine = ne(this.combine, this), this.normalize = ne(this.normalize, this), this.length = ne(this.length, this), this.cross = ne(this.cross, this), this.dot = ne(this.dot, this), this.e = ne(this.e, this)
        }
        return t.prototype.e = function (t) {
            return 1 > t || t > this.els.length ? null : this.els[t - 1]
        }, t.prototype.dot = function (t) {
            var e, n, r;
            if (e = t.els || t, r = 0, n = this.els.length, n !== e.length) return null;
            for (n += 1; --n;) r += this.els[n - 1] * e[n - 1];
            return r
        }, t.prototype.cross = function (e) {
            var n, r;
            return r = e.els || e, 3 !== this.els.length || 3 !== r.length ? null : (n = this.els, new t([n[1] * r[2] - n[2] * r[1], n[2] * r[0] - n[0] * r[2], n[0] * r[1] - n[1] * r[0]]))
        }, t.prototype.length = function () {
            var t, e, n, r, i;
            for (t = 0, i = this.els, n = 0, r = i.length; r > n; n++) e = i[n], t += Math.pow(e, 2);
            return Math.sqrt(t)
        }, t.prototype.normalize = function () {
            var e, n, r, i, o;
            r = this.length(), i = [], o = this.els;
            for (n in o) e = o[n], i[n] = e / r;
            return new t(i)
        }, t.prototype.combine = function (e, n, r) {
            var i, o, s, a;
            for (o = [], i = s = 0, a = this.els.length; a >= 0 ? a > s : s > a; i = a >= 0 ? ++s : --s) o[i] = n * this.els[i] + r * e.els[i];
            return new t(o)
        }, t
    }(), e = function () {
        function t() {
            this.toMatrix = ne(this.toMatrix, this), this.format = ne(this.format, this), this.interpolate = ne(this.interpolate, this)
        }
        return t.prototype.interpolate = function (e, n, r) {
            var i, o, s, a, l, u, f, h, c, p, m, d, g, y, v, b, w, x;
            for (null == r && (r = null), s = this, o = new t, w = ["translate", "scale", "skew", "perspective"], d = 0, b = w.length; b > d; d++)
                for (f = w[d], o[f] = [], a = g = 0, x = s[f].length - 1; x >= 0 ? x >= g : g >= x; a = x >= 0 ? ++g : --g) o[f][a] = null == r || r.indexOf(f) > -1 || r.indexOf("" + f + ["x", "y", "z"][a]) > -1 ? (e[f][a] - s[f][a]) * n + s[f][a] : s[f][a];
            if (null == r || -1 !== r.indexOf("rotate")) {
                if (h = s.quaternion, c = e.quaternion, i = h[0] * c[0] + h[1] * c[1] + h[2] * c[2] + h[3] * c[3], 0 > i) {
                    for (a = y = 0; 3 >= y; a = ++y) h[a] = -h[a];
                    i = -i
                }
                for (i + 1 > .05 ? 1 - i >= .05 ? (m = Math.acos(i), u = 1 / Math.sin(m), p = Math.sin(m * (1 - n)) * u, l = Math.sin(m * n) * u) : (p = 1 - n, l = n) : (c[0] = -h[1], c[1] = h[0], c[2] = -h[3], c[3] = h[2], p = Math.sin(piDouble * (.5 - n)), l = Math.sin(piDouble * n)), o.quaternion = [], a = v = 0; 3 >= v; a = ++v) o.quaternion[a] = h[a] * p + c[a] * l
            } else o.quaternion = s.quaternion;
            return o
        }, t.prototype.format = function () {
            return this.toMatrix().toString()
        }, t.prototype.toMatrix = function () {
            var t, e, n, r, i, o, s, l, u, f, h, c, p, m, d, g;
            for (t = this, i = a.I(4), e = p = 0; 3 >= p; e = ++p) i.els[e][3] = t.perspective[e];
            for (o = t.quaternion, f = o[0], h = o[1], c = o[2], u = o[3], s = t.skew, r = [
                    [1, 0],
                    [2, 0],
                    [2, 1]
                ], e = m = 2; m >= 0; e = --m) s[e] && (l = a.I(4), l.els[r[e][0]][r[e][1]] = s[e], i = i.multiply(l));
            for (i = i.multiply(new a([
                    [1 - 2 * (h * h + c * c), 2 * (f * h - c * u), 2 * (f * c + h * u), 0],
                    [2 * (f * h + c * u), 1 - 2 * (f * f + c * c), 2 * (h * c - f * u), 0],
                    [2 * (f * c - h * u), 2 * (h * c + f * u), 1 - 2 * (f * f + h * h), 0],
                    [0, 0, 0, 1]
                ])), e = d = 0; 2 >= d; e = ++d) {
                for (n = g = 0; 2 >= g; n = ++g) i.els[e][n] *= t.scale[e];
                i.els[3][e] = t.translate[e]
            }
            return i
        }, t
    }(), a = function () {
        function t(t) {
            this.els = t, this.toString = ne(this.toString, this), this.decompose = ne(this.decompose, this), this.inverse = ne(this.inverse, this), this.augment = ne(this.augment, this), this.toRightTriangular = ne(this.toRightTriangular, this), this.transpose = ne(this.transpose, this), this.multiply = ne(this.multiply, this), this.dup = ne(this.dup, this), this.e = ne(this.e, this)
        }
        return t.prototype.e = function (t, e) {
            return 1 > t || t > this.els.length || 1 > e || e > this.els[0].length ? null : this.els[t - 1][e - 1]
        }, t.prototype.dup = function () {
            return new t(this.els)
        }, t.prototype.multiply = function (e) {
            var n, r, i, o, s, a, l, u, f, h, c, p, m;
            for (p = e.modulus ? !0 : !1, n = e.els || e, "undefined" == typeof n[0][0] && (n = new t(n).els), h = this.els.length, l = h, u = n[0].length, i = this.els[0].length, o = [], h += 1; --h;)
                for (s = l - h, o[s] = [], c = u, c += 1; --c;) {
                    for (a = u - c, m = 0, f = i, f += 1; --f;) r = i - f, m += this.els[s][r] * n[r][a];
                    o[s][a] = m
                }
            return n = new t(o), p ? n.col(1) : n
        }, t.prototype.transpose = function () {
            var e, n, r, i, o, s, a;
            for (a = this.els.length, e = this.els[0].length, n = [], o = e, o += 1; --o;)
                for (r = e - o, n[r] = [], s = a, s += 1; --s;) i = a - s, n[r][i] = this.els[i][r];
            return new t(n)
        }, t.prototype.toRightTriangular = function () {
            var t, e, n, r, i, o, s, a, l, u, f, h, c, p;
            for (t = this.dup(), a = this.els.length, i = a, o = this.els[0].length; --a;) {
                if (n = i - a, 0 === t.els[n][n])
                    for (r = f = c = n + 1; i >= c ? i > f : f > i; r = i >= c ? ++f : --f)
                        if (0 !== t.els[r][n]) {
                            for (e = [], l = o, l += 1; --l;) u = o - l, e.push(t.els[n][u] + t.els[r][u]);
                            t.els[n] = e;
                            break
                        } if (0 !== t.els[n][n])
                    for (r = h = p = n + 1; i >= p ? i > h : h > i; r = i >= p ? ++h : --h) {
                        for (s = t.els[r][n] / t.els[n][n], e = [], l = o, l += 1; --l;) u = o - l, e.push(n >= u ? 0 : t.els[r][u] - t.els[n][u] * s);
                        t.els[r] = e
                    }
            }
            return t
        }, t.prototype.augment = function (e) {
            var n, r, i, o, s, a, l, u, f;
            if (n = e.els || e, "undefined" == typeof n[0][0] && (n = new t(n).els), r = this.dup(), i = r.els[0].length, u = r.els.length, a = u, l = n[0].length, u !== n.length) return null;
            for (u += 1; --u;)
                for (o = a - u, f = l, f += 1; --f;) s = l - f, r.els[o][i + s] = n[o][s];
            return r
        }, t.prototype.inverse = function () {
            var e, n, r, i, o, s, a, l, u, f, h, c, p;
            for (f = this.els.length, a = f, e = this.augment(t.I(f)).toRightTriangular(), l = e.els[0].length, o = [], f += 1; --f;) {
                for (i = f - 1, r = [], h = l, o[i] = [], n = e.els[i][i], h += 1; --h;) c = l - h, u = e.els[i][c] / n, r.push(u), c >= a && o[i].push(u);
                for (e.els[i] = r, s = p = 0; i >= 0 ? i > p : p > i; s = i >= 0 ? ++p : --p) {
                    for (r = [], h = l, h += 1; --h;) c = l - h, r.push(e.els[s][c] - e.els[i][c] * e.els[s][i]);
                    e.els[s] = r
                }
            }
            return new t(o)
        }, t.I = function (e) {
            var n, r, i, o, s;
            for (n = [], o = e, e += 1; --e;)
                for (r = o - e, n[r] = [], s = o, s += 1; --s;) i = o - s, n[r][i] = r === i ? 1 : 0;
            return new t(n)
        }, t.prototype.decompose = function () {
            var t, n, r, i, o, s, a, l, u, h, c, p, m, d, g, y, v, b, w, x, M, k, S, T, C, F, H, R, q, X, Y, j, z, I, A, G, V, Z;
            for (s = this, x = [], v = [], b = [], h = [], l = [], t = [], n = q = 0; 3 >= q; n = ++q)
                for (t[n] = [], i = X = 0; 3 >= X; i = ++X) t[n][i] = s.els[n][i];
            if (0 === t[3][3]) return !1;
            for (n = Y = 0; 3 >= Y; n = ++Y)
                for (i = j = 0; 3 >= j; i = ++j) t[n][i] /= t[3][3];
            for (u = s.dup(), n = z = 0; 2 >= z; n = ++z) u.els[n][3] = 0;
            if (u.els[3][3] = 1, 0 !== t[0][3] || 0 !== t[1][3] || 0 !== t[2][3]) {
                for (p = new f(t.slice(0, 4)[3]), r = u.inverse(), M = r.transpose(), l = M.multiply(p).els, n = I = 0; 2 >= I; n = ++I) t[n][3] = 0;
                t[3][3] = 1
            } else l = [0, 0, 0, 1];
            for (n = A = 0; 2 >= A; n = ++A) x[n] = t[3][n], t[3][n] = 0;
            for (d = [], n = G = 0; 2 >= G; n = ++G) d[n] = new f(t[n].slice(0, 3));
            if (v[0] = d[0].length(), d[0] = d[0].normalize(), b[0] = d[0].dot(d[1]), d[1] = d[1].combine(d[0], 1, -b[0]), v[1] = d[1].length(), d[1] = d[1].normalize(), b[0] /= v[1], b[1] = d[0].dot(d[2]), d[2] = d[2].combine(d[0], 1, -b[1]), b[2] = d[1].dot(d[2]), d[2] = d[2].combine(d[1], 1, -b[2]), v[2] = d[2].length(), d[2] = d[2].normalize(), b[1] /= v[2], b[2] /= v[2], a = d[1].cross(d[2]), d[0].dot(a) < 0)
                for (n = V = 0; 2 >= V; n = ++V)
                    for (v[n] *= -1, i = Z = 0; 2 >= Z; i = ++Z) d[n].els[i] *= -1;
            g = function (t, e) {
                return d[t].els[e]
            }, m = [], m[1] = Math.asin(-g(0, 2)), 0 !== Math.cos(m[1]) ? (m[0] = Math.atan2(g(1, 2), g(2, 2)), m[2] = Math.atan2(g(0, 1), g(0, 0))) : (m[0] = Math.atan2(-g(2, 0), g(1, 1)), m[1] = 0), w = g(0, 0) + g(1, 1) + g(2, 2) + 1, w > 1e-4 ? (y = .5 / Math.sqrt(w), C = .25 / y, F = (g(2, 1) - g(1, 2)) * y, H = (g(0, 2) - g(2, 0)) * y, R = (g(1, 0) - g(0, 1)) * y) : g(0, 0) > g(1, 1) && g(0, 0) > g(2, 2) ? (y = 2 * Math.sqrt(1 + g(0, 0) - g(1, 1) - g(2, 2)), F = .25 * y, H = (g(0, 1) + g(1, 0)) / y, R = (g(0, 2) + g(2, 0)) / y, C = (g(2, 1) - g(1, 2)) / y) : g(1, 1) > g(2, 2) ? (y = 2 * Math.sqrt(1 + g(1, 1) - g(0, 0) - g(2, 2)), F = (g(0, 1) + g(1, 0)) / y, H = .25 * y, R = (g(1, 2) + g(2, 1)) / y, C = (g(0, 2) - g(2, 0)) / y) : (y = 2 * Math.sqrt(1 + g(2, 2) - g(0, 0) - g(1, 1)), F = (g(0, 2) + g(2, 0)) / y, H = (g(1, 2) + g(2, 1)) / y, R = .25 * y, C = (g(1, 0) - g(0, 1)) / y), h = [F, H, R, C], c = new e, c.translate = x, c.scale = v, c.skew = b, c.quaternion = h, c.perspective = l, c.rotate = m;
            for (S in c) {
                k = c[S];
                for (o in k) T = k[o], isNaN(T) && (k[o] = 0)
            }
            return c
        }, t.prototype.toString = function () {
            var t, e, n, r, i;
            for (n = "matrix3d(", t = r = 0; 3 >= r; t = ++r)
                for (e = i = 0; 3 >= i; e = ++i) n += Z(this.els[t][e], 10), (3 !== t || 3 !== e) && (n += ",");
            return n += ")"
        }, t.matrixForTransform = b(function (t) {
            var e, n, r, i, o, s;
            return e = document.createElement("div"), e.style.position = "absolute", e.style.visibility = "hidden", e.style[A("transform")] = t, document.body.appendChild(e), r = window.getComputedStyle(e, null), n = null != (i = null != (o = r.transform) ? o : r[A("transform")]) ? i : null != (s = T.tests) ? s.matrixForTransform(t) : void 0, document.body.removeChild(e), n
        }), t.fromTransform = function (e) {
            var n, r, i, o, s, a;
            for (o = null != e ? e.match(/matrix3?d?\(([-0-9,e \.]*)\)/) : void 0, o ? (n = o[1].split(","), n = n.map(parseFloat), r = 6 === n.length ? [n[0], n[1], 0, 0, n[2], n[3], 0, 0, 0, 0, 1, 0, n[4], n[5], 0, 1] : n) : r = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], s = [], i = a = 0; 3 >= a; i = ++a) s.push(r.slice(4 * i, 4 * i + 4));
            return new t(s)
        }, t
    }(), I = b(function (t) {
        var e, n, r, i, o, s, a, l, u, f;
        if (void 0 !== document.body.style[t]) return "";
        for (i = t.split("-"), o = "", s = 0, l = i.length; l > s; s++) r = i[s], o += r.substring(0, 1).toUpperCase() + r.substring(1);
        for (f = ["Webkit", "Moz", "ms"], a = 0, u = f.length; u > a; a++)
            if (n = f[a], e = n + o, void 0 !== document.body.style[e]) return n;
        return ""
    }), A = b(function (t) {
        var e;
        return e = I(t), "Moz" === e ? "" + e + (t.substring(0, 1).toUpperCase() + t.substring(1)) : "" !== e ? "-" + e.toLowerCase() + "-" + Q(t) : Q(t)
    }), V = "undefined" != typeof window && null !== window ? window.requestAnimationFrame : void 0, p = [], m = [], P = !1, W = 1, "undefined" != typeof window && null !== window && window.addEventListener("keyup", function (t) {
        return 68 === t.keyCode && t.shiftKey && t.ctrlKey ? T.toggleSlow() : void 0
    }), null == V && (q = 0, V = function (t) {
        var e, n, r;
        return e = Date.now(), r = Math.max(0, 16 - (e - q)), n = window.setTimeout(function () {
            return t(e + r)
        }, r), q = e + r, n
    }), O = !1, E = !1, $ = function () {
        return O ? void 0 : (O = !0, V(L))
    }, L = function (t) {
        var e, n, r, i;
        if (E) return void V(L);
        for (n = [], r = 0, i = p.length; i > r; r++) e = p[r], c(t, e) || n.push(e);
        return p = p.filter(function (t) {
            return -1 === n.indexOf(t)
        }), 0 === p.length ? O = !1 : V(L)
    }, c = function (t, e) {
        var n, r, i, o, s, a, l, u;
        if (null == e.tStart && (e.tStart = t), o = (t - e.tStart) / e.options.duration, s = e.curve(o), r = {}, o >= 1) r = e.curve.initialForce ? e.properties.start : e.properties.end;
        else {
            u = e.properties.start;
            for (n in u) i = u[n], r[n] = F(i, e.properties.end[n], s)
        }
        return g(e.el, r), "function" == typeof (a = e.options).change && a.change(e.el), o >= 1 && "function" == typeof (l = e.options).complete && l.complete(e.el), 1 > o
    }, F = function (t, e, n) {
        return null != t && null != t.interpolate ? t.interpolate(e, n) : null
    }, N = function (t, e, n, r) {
        var i, o, u, f, h, c, d, g;
        if (null != r && (m = m.filter(function (t) {
                return t.id !== r
            })), T.stop(t, {
                timeout: !1
            }), !n.animated) return T.css(t, e), void("function" == typeof n.complete && n.complete(this));
        e = z(e), h = C(t, Object.keys(e)), i = {}, c = [];
        for (u in e) d = e[u], _.contains(u) ? c.push([u, d]) : (i[u] = M(d), i[u] instanceof s && null != t.style && (i[u].prefix = "", null == (g = i[u]).suffix && (g.suffix = ee(u, 0))));
        return c.length > 0 && (o = R(t), o ? (f = new l, f.applyProperties(c)) : (d = c.map(function (t) {
            return te(t[0], t[1])
        }).join(" "), f = a.fromTransform(a.matrixForTransform(d))), i.transform = f.decompose(), o && h.transform.applyRotateCenter([i.transform.props.rotate[1], i.transform.props.rotate[2]])), p.push({
            el: t,
            properties: {
                start: h,
                end: i
            },
            options: n,
            curve: n.type.call(n.type, n)
        }), $()
    }, J = [], K = 0, D = function (t) {
        return H() ? t.realTimeoutId = setTimeout(function () {
            return t.fn(), w(t.id)
        }, t.delay) : void 0
    }, h = function (t, e) {
        var n;
        return K += 1, n = {
            id: K,
            tStart: Date.now(),
            fn: t,
            delay: e,
            originalDelay: e
        }, D(n), J.push(n), K
    }, w = function (t) {
        return J = J.filter(function (e) {
            return e.id === t && clearTimeout(e.realTimeoutId), e.id !== t
        })
    }, X = function (t, e) {
        var n;
        return null != t ? (n = t - e.tStart, e.originalDelay - n) : e.originalDelay
    }, "undefined" != typeof window && null !== window && window.addEventListener("unload", function () {}), U = null, j(function (t) {
        var e, n, r, i, o, s, a, l, u, f;
        if (E = !t, t) {
            if (O)
                for (n = Date.now() - U, o = 0, l = p.length; l > o; o++) e = p[o], null != e.tStart && (e.tStart += n);
            for (s = 0, u = J.length; u > s; s++) r = J[s], r.delay = X(U, r), D(r);
            return U = null
        }
        for (U = Date.now(), f = [], i = 0, a = J.length; a > i; i++) r = J[i], f.push(clearTimeout(r.realTimeoutId));
        return f
    }), T = {}, T.linear = function () {
        return function (t) {
            return t
        }
    }, T.spring = function (t) {
        var e, n, r, i, o, s;
        return null == t && (t = {}), d(t, arguments.callee.defaults), i = Math.max(1, t.frequency / 20), o = Math.pow(20, t.friction / 100), s = t.anticipationSize / 1e3, r = Math.max(0, s), e = function (e) {
                var n, r, i, o, a;
                return n = .8, o = s / (1 - s), a = 0, i = (o - n * a) / (o - a), r = (n - i) / o, r * e * t.anticipationStrength / 100 + i
            }, n = function (t) {
                return Math.pow(o / 10, -t) * (1 - t)
            },
            function (t) {
                var r, o, a, l, u, f, h, c;
                return f = t / (1 - s) - s / (1 - s), s > t ? (c = s / (1 - s) - s / (1 - s), h = 0 / (1 - s) - s / (1 - s), u = Math.acos(1 / e(c)), a = (Math.acos(1 / e(h)) - u) / (i * -s), r = e) : (r = n, u = 0, a = 1), o = r(f), l = i * (t - s) * a + u, 1 - o * Math.cos(l)
            }
    }, T.bounce = function (t) {
        var e, n, r, i;
        return null == t && (t = {}), d(t, arguments.callee.defaults), r = Math.max(1, t.frequency / 20), i = Math.pow(20, t.friction / 100), e = function (t) {
            return Math.pow(i / 10, -t) * (1 - t)
        }, n = function (t) {
            var n, i, o, s;
            return s = -1.57, i = 1, n = e(t), o = r * t * i + s, n * Math.cos(o)
        }, n.initialForce = !0, n
    }, T.gravity = function (t) {
        var e, n, r, i, o, s, a;
        return null == t && (t = {}), d(t, arguments.callee.defaults), n = Math.min(t.bounciness / 1250, .8), i = t.elasticity / 1e3, a = 100, r = [], e = function () {
                var r, i;
                for (r = Math.sqrt(2 / a), i = {
                        a: -r,
                        b: r,
                        H: 1
                    }, t.initialForce && (i.a = 0, i.b = 2 * i.b); i.H > .001;) e = i.b - i.a, i = {
                    a: i.b,
                    b: i.b + e * n,
                    H: i.H * n * n
                };
                return i.b
            }(), s = function (n, r, i, o) {
                var s, a;
                return e = r - n, a = 2 / e * o - 1 - 2 * n / e, s = a * a * i - i + 1, t.initialForce && (s = 1 - s), s
            },
            function () {
                var o, s, l, u;
                for (s = Math.sqrt(2 / (a * e * e)), l = {
                        a: -s,
                        b: s,
                        H: 1
                    }, t.initialForce && (l.a = 0, l.b = 2 * l.b), r.push(l), o = e, u = []; l.b < 1 && l.H > .001;) o = l.b - l.a, l = {
                    a: l.b,
                    b: l.b + o * n,
                    H: l.H * i
                }, u.push(r.push(l));
                return u
            }(), o = function (e) {
                var n, i, o;
                for (i = 0, n = r[i]; !(e >= n.a && e <= n.b) && (i += 1, n = r[i]););
                return o = n ? s(n.a, n.b, n.H, e) : t.initialForce ? 0 : 1
            }, o.initialForce = t.initialForce, o
    }, T.forceWithGravity = function (t) {
        return null == t && (t = {}), d(t, arguments.callee.defaults), t.initialForce = !0, T.gravity(t)
    }, T.bezier = function () {
        var t, e, n;
        return e = function (t, e, n, r, i) {
                return Math.pow(1 - t, 3) * e + 3 * Math.pow(1 - t, 2) * t * n + 3 * (1 - t) * Math.pow(t, 2) * r + Math.pow(t, 3) * i
            }, t = function (t, n, r, i, o) {
                return {
                    x: e(t, n.x, r.x, i.x, o.x),
                    y: e(t, n.y, r.y, i.y, o.y)
                }
            }, n = function (t, e, n) {
                var r, i, o, s, a, l, u, f, h, c;
                for (r = null, h = 0, c = e.length; c > h && (i = e[h], t >= i(0).x && t <= i(1).x && (r = i), null === r); h++);
                if (!r) return n ? 0 : 1;
                for (f = 1e-4, s = 0, l = 1, a = (l + s) / 2, u = r(a).x, o = 0; Math.abs(t - u) > f && 100 > o;) t > u ? s = a : l = a, a = (l + s) / 2, u = r(a).x, o += 1;
                return r(a).y
            },
            function (e) {
                var r, i, o;
                return null == e && (e = {}), i = e.points, o = !1, r = function () {
                        var e, n, o;
                        r = [], o = function (e, n) {
                            var i;
                            return i = function (r) {
                                return t(r, e, e.cp[e.cp.length - 1], n.cp[0], n)
                            }, r.push(i)
                        };
                        for (e in i) {
                            if (n = parseInt(e), n >= i.length - 1) break;
                            o(i[n], i[n + 1])
                        }
                        return r
                    }(),
                    function (t) {
                        return 0 === t ? 0 : 1 === t ? 1 : n(t, r, o)
                    }
            }
    }(), T.easeInOut = function (t) {
        var e, n;
        return null == t && (t = {}), e = null != (n = t.friction) ? n : arguments.callee.defaults.friction, T.bezier({
            points: [{
                x: 0,
                y: 0,
                cp: [{
                    x: .92 - e / 1e3,
                    y: 0
                }]
            }, {
                x: 1,
                y: 1,
                cp: [{
                    x: .08 + e / 1e3,
                    y: 1
                }]
            }]
        })
    }, T.easeIn = function (t) {
        var e, n;
        return null == t && (t = {}), e = null != (n = t.friction) ? n : arguments.callee.defaults.friction, T.bezier({
            points: [{
                x: 0,
                y: 0,
                cp: [{
                    x: .92 - e / 1e3,
                    y: 0
                }]
            }, {
                x: 1,
                y: 1,
                cp: [{
                    x: 1,
                    y: 1
                }]
            }]
        })
    }, T.easeOut = function (t) {
        var e, n;
        return null == t && (t = {}), e = null != (n = t.friction) ? n : arguments.callee.defaults.friction, T.bezier({
            points: [{
                x: 0,
                y: 0,
                cp: [{
                    x: 0,
                    y: 0
                }]
            }, {
                x: 1,
                y: 1,
                cp: [{
                    x: .08 + e / 1e3,
                    y: 1
                }]
            }]
        })
    }, T.spring.defaults = {
        frequency: 300,
        friction: 200,
        anticipationSize: 0,
        anticipationStrength: 0
    }, T.bounce.defaults = {
        frequency: 300,
        friction: 200
    }, T.forceWithGravity.defaults = T.gravity.defaults = {
        bounciness: 400,
        elasticity: 200
    }, T.easeInOut.defaults = T.easeIn.defaults = T.easeOut.defaults = {
        friction: 500
    }, T.css = Y(function (t, e) {
        return y(t, e, !0)
    }), T.animate = Y(function (t, e, n) {
        var r;
        return null == n && (n = {}), n = x(n), d(n, {
            type: T.easeInOut,
            duration: 1e3,
            delay: 0,
            animated: !0
        }), n.duration = Math.max(0, n.duration * W), n.delay = Math.max(0, n.delay), 0 === n.delay ? N(t, e, n) : (r = T.setTimeout(function () {
            return N(t, e, n, r)
        }, n.delay), m.push({
            id: r,
            el: t
        }))
    }), T.stop = Y(function (t, e) {
        return null == e && (e = {}), null == e.timeout && (e.timeout = !0), e.timeout && (m = m.filter(function (n) {
            return n.el !== t || null != e.filter && !e.filter(n) ? !1 : (T.clearTimeout(n.id), !0)
        })), p = p.filter(function (e) {
            return e.el !== t
        })
    }), T.setTimeout = function (t, e) {
        return h(t, e * W)
    }, T.clearTimeout = function (t) {
        return w(t)
    }, T.toggleSlow = function () {
        return P = !P, W = P ? 3 : 1, "undefined" != typeof console && null !== console && "function" == typeof console.log ? console.log("dynamics.js: slow animations " + (P ? "enabled" : "disabled")) : void 0
    }, "object" == typeof module && "object" == typeof module.exports ? module.exports = T : "function" == typeof define ? define("dynamics", function () {
        return T
    }) : window.dynamics = T
}).call(this);

function mobilecheck() {
	var check = false;
	(function (a) { if (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
	return check;
}

var clickeventtype = mobilecheck() ? 'touchstart' : 'click';

(function () {
	var support = { animations: Modernizr.cssanimations },
		animEndEventNames = { 'WebkitAnimation': 'webkitAnimationEnd', 'OAnimation': 'oAnimationEnd', 'msAnimation': 'MSAnimationEnd', 'animation': 'animationend' },
		animEndEventName = animEndEventNames[Modernizr.prefixed('animation')],
		onEndAnimation = function (el, callback) {
			var onEndCallbackFn = function (ev) {
				if (support.animations) {
					if (ev.target != this) return;
					this.removeEventListener(animEndEventName, onEndCallbackFn);
				}
				if (callback && typeof callback === 'function') { callback.call(); }
			};
			if (support.animations) {
				el.addEventListener(animEndEventName, onEndCallbackFn);
			}
			else {
				onEndCallbackFn();
			}
		};

	[].slice.call(document.querySelectorAll('.button--sonar')).forEach(function (el) {
		el.addEventListener(clickeventtype, function (ev) {
			if (el.getAttribute('data-state') !== 'locked') {
				classie.add(el, 'button--active');
				onEndAnimation(el, function () {
					classie.remove(el, 'button--active');
				});
			}
		});
	});
})();

;(function(window) {

	'use strict';

	var support = { animations : Modernizr.cssanimations },
		animEndEventNames = { 'WebkitAnimation' : 'webkitAnimationEnd', 'OAnimation' : 'oAnimationEnd', 'msAnimation' : 'MSAnimationEnd', 'animation' : 'animationend' },
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		onEndAnimation = function( el, callback ) {
			var onEndCallbackFn = function( ev ) {
				if( support.animations ) {
					if( ev.target != this ) return;
					this.removeEventListener( animEndEventName, onEndCallbackFn );
				}
				if( callback && typeof callback === 'function' ) { callback.call(); }
			};
			if( support.animations ) {
				el.addEventListener( animEndEventName, onEndCallbackFn );
			}
			else {
				onEndCallbackFn();
			}
		};

	function extend( a, b ) {
		for( var key in b ) {
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	function Stack(el, options) {
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );
		this.items = [].slice.call(this.el.children);
		this.itemsTotal = this.items.length;
		if( this.options.infinite && this.options.visible >= this.itemsTotal || !this.options.infinite && this.options.visible > this.itemsTotal || this.options.visible <=0 ) {
			this.options.visible = 1;
		}
		this.current = 0;
		this._init();
	}

	Stack.prototype.options = {
		// stack's perspective value
		perspective: 1000,
		// stack's perspective origin
		perspectiveOrigin : '50% -50%',
		// number of visible items in the stack
		visible : 3,
		// infinite navigation
		infinite : true,
		// callback: when reaching the end of the stack
		onEndStack : function() {return false;},
		// animation settings for the items' movements in the stack when the items rearrange
		// object that is passed to the dynamicsjs animate function (see more at http://dynamicsjs.com/)
		// example:
		// {type: dynamics.spring,duration: 1641,frequency: 557,friction: 459,anticipationSize: 206,anticipationStrength: 392}
		stackItemsAnimation : {
			duration : 500,
			type : dynamics.bezier,
			points : [{'x':0,'y':0,'cp':[{'x':0.25,'y':0.1}]},{'x':1,'y':1,'cp':[{'x':0.25,'y':1}]}]
		},
		// delay for the items' rearrangement / delay before stackItemsAnimation is applied
		stackItemsAnimationDelay : 0,
		// animation settings for the items' movements in the stack before the rearrangement
		// we can set up different settings depending on whether we are approving or rejecting an item
		/*
		stackItemsPreAnimation : {
			reject : {
				// if true, then the settings.properties parameter will be distributed through the items in a non equal fashion
				// for instance, if we set settings.properties = {translateX:100} and we have options.visible = 4,
				// then the second item in the stack will translate 100px, the second one 75px and the third 50px
				elastic : true,
				// object that is passed into the dynamicsjs animate function - second parameter -  (see more at http://dynamicsjs.com/)
				animationProperties : {},
				// object that is passed into the dynamicsjs animate function - third parameter - (see more at http://dynamicsjs.com/)
				animationSettings : {}
			},
			accept : {
				// if true, then the settings.properties parameter will be distributed through the items in a non equal fashion
				// for instance, if we set settings.properties = {translateX:100} and we have options.visible = 4,
				// then the second item on the stack will translate 100px, the second one 75px and the third 50px
				elastic : true,
				// object that is passed into the dynamicsjs animate function - second parameter -  (see more at http://dynamicsjs.com/)
				animationProperties : {},
				// object that is passed into the dynamicsjs animate function (see more at http://dynamicsjs.com/)
				animationSettings : {}
			}
		}
		*/
	};

	// set the initial styles for the visible items
	Stack.prototype._init = function() {
		// set default styles
		// first, the stack
		this.el.style.WebkitPerspective = this.el.style.perspective = this.options.perspective + 'px';
		this.el.style.WebkitPerspectiveOrigin = this.el.style.perspectiveOrigin = this.options.perspectiveOrigin;

		var self = this;

		// the items
		for(var i = 0; i < this.itemsTotal; ++i) {
			var item = this.items[i];
			if( i < this.options.visible ) {
				item.style.opacity = 1;
				item.style.pointerEvents = 'auto';
				item.style.zIndex = i === 0 ? parseInt(this.options.visible + 1) : parseInt(this.options.visible - i);
				item.style.WebkitTransform = item.style.transform = 'translate3d(0px, 0px, ' + parseInt(-1 * 50 * i) + 'px)';
			}
			else {
				item.style.WebkitTransform = item.style.transform = 'translate3d(0,0,-' + parseInt(this.options.visible * 50) + 'px)';
			}
		}

		classie.add(this.items[this.current], 'stack__item--current');
	};

	Stack.prototype.reject = function(callback) {
		this._next('reject', callback);
	};

	Stack.prototype.accept = function(callback) {
		this._next('accept', callback);
	};

	Stack.prototype.restart = function() {
		this.hasEnded = false;
		this._init();
	};

	Stack.prototype._next = function(action, callback) {
		if( this.isAnimating || ( !this.options.infinite && this.hasEnded ) ) return;
		this.isAnimating = true;

		// current item
		var currentItem = this.items[this.current];
		classie.remove(currentItem, 'stack__item--current');

		// add animation class
		classie.add(currentItem, action === 'accept' ? 'stack__item--accept' : 'stack__item--reject');

		var self = this;
		onEndAnimation(currentItem, function() {
			// reset current item
			currentItem.style.opacity = 0;
			currentItem.style.pointerEvents = 'none';
			currentItem.style.zIndex = -1;
			currentItem.style.WebkitTransform = currentItem.style.transform = 'translate3d(0px, 0px, -' + parseInt(self.options.visible * 50) + 'px)';

			classie.remove(currentItem, action === 'accept' ? 'stack__item--accept' : 'stack__item--reject');

			self.items[self.current].style.zIndex = self.options.visible + 1;
			self.isAnimating = false;

			if( callback ) callback();

			if( !self.options.infinite && self.current === 0 ) {
				self.hasEnded = true;
				// callback
				self.options.onEndStack(self);
			}
		});

		// set style for the other items
		for(var i = 0; i < this.itemsTotal; ++i) {
			if( i >= this.options.visible ) break;

			if( !this.options.infinite ) {
				if( this.current + i >= this.itemsTotal - 1 ) break;
				var pos = this.current + i + 1;
			}
			else {
				var pos = this.current + i < this.itemsTotal - 1 ? this.current + i + 1 : i - (this.itemsTotal - this.current - 1);
			}

			var item = this.items[pos],
				// stack items animation
				animateStackItems = function(item, i) {
					item.style.pointerEvents = 'auto';
					item.style.opacity = 1;
					item.style.zIndex = parseInt(self.options.visible - i);

					dynamics.animate(item, {
						translateZ : parseInt(-1 * 50 * i)
					}, self.options.stackItemsAnimation);
				};

			setTimeout(function(item,i) {
				return function() {
					var preAnimation;

					if( self.options.stackItemsPreAnimation ) {
						preAnimation = action === 'accept' ? self.options.stackItemsPreAnimation.accept : self.options.stackItemsPreAnimation.reject;
					}

					if( preAnimation ) {
						// items "pre animation" properties
						var animProps = {};

						for (var key in preAnimation.animationProperties) {
							var interval = preAnimation.elastic ? preAnimation.animationProperties[key]/self.options.visible : 0;
							animProps[key] = preAnimation.animationProperties[key] - Number(i*interval);
						}

						// this one remains the same..
						animProps.translateZ = parseInt(-1 * 50 * (i+1));

						preAnimation.animationSettings.complete = function() {
							animateStackItems(item, i);
						};

						dynamics.animate(item, animProps, preAnimation.animationSettings);
					}
					else {
						animateStackItems(item, i);
					}
				};
			}(item,i), this.options.stackItemsAnimationDelay);
		}

		// update current
		this.current = this.current < this.itemsTotal - 1 ? this.current + 1 : 0;
		classie.add(this.items[this.current], 'stack__item--current');
	}

	window.Stack = Stack;

})(window);

(function () {

	var support = { animations: Modernizr.cssanimations },
		animEndEventNames = { 'WebkitAnimation': 'webkitAnimationEnd', 'OAnimation': 'oAnimationEnd', 'msAnimation': 'MSAnimationEnd', 'animation': 'animationend' },
		animEndEventName = animEndEventNames[Modernizr.prefixed('animation')],
		onEndAnimation = function (el, callback) {
			var onEndCallbackFn = function (ev) {
				if (support.animations) {
					if (ev.target != this) return;
					this.removeEventListener(animEndEventName, onEndCallbackFn);
				}
				if (callback && typeof callback === 'function') { callback.call(); }
			};
			if (support.animations) {
				el.addEventListener(animEndEventName, onEndCallbackFn);
			}
			else {
				onEndCallbackFn();
			}
		};

	function nextSibling(el) {
		var nextSibling = el.nextSibling;
		while (nextSibling && nextSibling.nodeType != 1) {
			nextSibling = nextSibling.nextSibling
		}
		return nextSibling;
	}

	var mawar = new Stack(document.getElementById('stack_mawar'), {
		perspective: 1500,
		perspectiveOrigin: '-150% 50%',
		visible: 3,
		stackItemsAnimation: {
			duration: 1300,
			type: dynamics.spring
		},
		stackItemsPreAnimation: {
			accept: {
				elastic: true,
				animationProperties: { translateX: 100 },
				animationSettings: {
					duration: 200,
					type: dynamics.easeIn
				}
			},
			reject: {
				elastic: true,
				animationProperties: { translateX: -100 },
				animationSettings: {
					duration: 200,
					type: dynamics.easeIn
				}
			}
		}
	});

	// controls the click ring effect on the button
	var buttonClickCallback = function (bttn) {
		var bttn = bttn || this;
		bttn.setAttribute('data-state', 'unlocked');
	};

	document.querySelector('.button--accept[data-stack = stack_mawar]').addEventListener(clickeventtype, function () { mawar.accept(buttonClickCallback.bind(this)); });
	// document.querySelector('.button--reject[data-stack = stack_mawar]').addEventListener(clickeventtype, function () { mawar.reject(buttonClickCallback.bind(this)); });

	[].slice.call(document.querySelectorAll('.button--sonar')).forEach(function (bttn) {
		bttn.addEventListener(clickeventtype, function () {
			bttn.setAttribute('data-state', 'locked');
		});
	});

	[].slice.call(document.querySelectorAll('.button--material')).forEach(function (bttn) {
		var radialAction = nextSibling(bttn.parentNode);

		bttn.addEventListener(clickeventtype, function (ev) {
			var boxOffset = radialAction.parentNode.getBoundingClientRect(),
				offset = bttn.getBoundingClientRect();

			radialAction.style.left = Number(offset.left - boxOffset.left) + 'px';
			radialAction.style.top = Number(offset.top - boxOffset.top) + 'px';

			classie.add(radialAction, classie.has(bttn, 'button--reject') ? 'material-circle--reject' : 'material-circle--accept');
			classie.add(radialAction, 'material-circle--active');
			onEndAnimation(radialAction, function () {
				classie.remove(radialAction, classie.has(bttn, 'button--reject') ? 'material-circle--reject' : 'material-circle--accept');
				classie.remove(radialAction, 'material-circle--active');
			});
		});
	});
})();



$(function () {
    $(".owl-carousel").owlCarousel({
        loop: true,
        center: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        stagePadding: 120,
        nav: true,
        navContainer: '.nav-container',
        responsive: {
            0: {
                items: 1,
                stagePadding: 30,
            },
            480: {
                items: 1,
                stagePadding: 30,
            },
            768: {
                items: 2,
                stagePadding: 30,
            },
            1366: {
                items: 4
            },
            1920: {
                items: 4
            }
        }
    });

    $('.video-play-btn').magnificPopup({
        type: 'iframe',
        disableOn: 700,
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: true
    });
});
